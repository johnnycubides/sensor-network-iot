#include "semva.h"

void semva_setup_gpio()
{
  mgos_gpio_setup_output(USER_LED, 0);
  mgos_gpio_setup_output(PIN_DONE, 0);
}

void semva_sleep()
{
  mgos_gpio_write(PIN_DONE, true);
}

void user_led_toggle()
{
  mgos_gpio_toggle(USER_LED);
}

void user_led_on()
{
  mgos_gpio_write(USER_LED, true);
}

void user_led_off()
{
  mgos_gpio_write(USER_LED, false);
}
