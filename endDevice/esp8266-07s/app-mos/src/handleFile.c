#include "handleFile.h"

const char *file_data = "data.json";
const char *file_config = "devConf.json";

bool readFile(const char *filename, char ** data)
{
  uint8_t i = 0;
  // se hacen 2 intentos de leer la memoria flash
  for(i=0; i<ATTEMPS_READ_FILE; i++)
  {
    *data = json_fread(filename);
    if(*data != NULL){
      LOG(LL_INFO, ("Archivo leido"));
      printf("data: %s", *data);
      return true;
    }
    i++;
  }
  LOG(LL_DEBUG,("Fail readfile"));
  return false;
}

/* static void scan_float_array(const char* str, int len, void* user_data) */

/* static void scan_array(const char *str, int len, void *user_data) */
/* { */
/*   struct json_token t; */
/*   int i; */
/*   LOG(LL_DEBUG,("now scan_array")); */
/*   /1* printf("Parsing array: %.*s\n", len, str); *1/ */
/*   for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) { */
/*     LOG(LL_DEBUG,("now json_scanf_array_elem")); */
/*     /1* char* pEnd; *1/ */
/*     char* pEnd = NULL; */
/*     float temp = strtof(t.ptr, pEnd); */
/*     printf("float %f", temp); */
/*     printf("Index %d, token [%.*s]\n", i, t.len, t.ptr); */
/*   } */
/* } */

/* static void scan_array_uint8_t(const char *str, int len, uint8_t data[]) */
/* { */
/*   struct json_token t; */
/*   uint8_t i; */
/*   LOG(LL_DEBUG,("now scan_array")); */
/*   /1* Cargar desde el segundo dato i=1 *1/ */
/*   for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) { */
/*     LOG(LL_DEBUG,("now json_scanf_array_elem")); */
/*     char* pEnd = NULL; */
/*     if(i < (STACK_SIZE-1)) */
/*     { */
/*       data[i+1] = (uint8_t)strtol(t.ptr, pEnd, 10); */
/*       printf("uint8_t %d\n", data[i+1]); */
/*     } */
/*     printf("Index %d, token [%.*s]\n", i, t.len, t.ptr); */
/*   } */
/* } */



static void scan_array_int8_t(const char *str, int len, int8_t data[])
{
  struct json_token t;
  uint8_t i;
  LOG(LL_DEBUG,("now scan_array"));
  /* Cargar desde el segundo dato i=1 */
  for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) {
    LOG(LL_DEBUG,("now json_scanf_array_elem"));
    char* pEnd = NULL;
    if(i < (STACK_SIZE-1))
    {
      data[i+1] = (int8_t)strtol(t.ptr, pEnd, 10);
      printf("int8_t %d\n", data[i+1]);
    }
    printf("Index %d, token [%.*s]\n", i, t.len, t.ptr);
  }
}

static void scan_array_float(const char *str, int len, float data[])
{
  struct json_token t;
  int i;
  LOG(LL_DEBUG,("now scan_array"));
  //TODO: de ésta manera hay corrimeinto de dato y se va perdiendo el último dato
  //lo cual es deseado, pero podría mejorar el algoritmo
  for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) {
    LOG(LL_DEBUG,("now json_scanf_array_elem"));
    char* pEnd = NULL;
    if(i < (STACK_SIZE-1))
    {
      data[i+1] = strtof(t.ptr, pEnd);
      printf("float %f", data[i+1]);
    }
    printf("Index %d, token [%.*s]\n", i, t.len, t.ptr);
  }
}

static void scan_array_int32_t(const char *str, int len, int32_t data[])
{
  struct json_token t;
  uint8_t i;
  LOG(LL_DEBUG,("now scan_array"));
  //TODO: de ésta manera hay corrimeinto de dato y se va perdiendo el último dato
  //lo cual es deseado, pero podría mejorar el algoritmo
  for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) {
    LOG(LL_DEBUG,("now json_scanf_array_elem"));
    char* pEnd = NULL;
    if(i < (STACK_SIZE-1))
    {
      data[i+1] = (int32_t)strtol(t.ptr, pEnd, 10);
      printf("uint32_t %d\n", data[i+1]);
    }
    printf("Index %d, token [%.*s]\n", i, t.len, t.ptr);
  }
}

bool getMeasurement2Stack(struct MeasurementStack *pMeasurementStack)
{
  LOG(LL_INFO, ("Enter in getMeasurement2Stack()"));
  if (!readFile(file_data, &data))
    return false; // No fue exitosa la lectura de los datos
  /* void *my_data = NULL; */
  LOG(LL_INFO, ("now json_scanf"));
  /* printf("data: %s", data); */
  /* if (json_scanf(data, strlen(data), "{ rm:%M }", scan_float_array, my_data) < 0) */
  /* json_scanf(data, strlen(data), "{ rm:%M }", scan_array, my_data); */
  if(data == NULL)
    return false; //Data en realidad está vacio
  /* if (json_scanf(data, strlen(data), "{ rm:%M }", scan_array, my_data) < 0) */
  // rm: humedad relativa
  if (json_scanf(data, strlen(data), "{ rm:%M }", scan_array_float, 
        pMeasurementStack->relativeMoisture) < 0)
    return false;
  // rt: temperatura relativa
  if (json_scanf(data, strlen(data), "{ rt:%M }", scan_array_float, 
        pMeasurementStack->relativeTemperature) < 0)
    return false;
  // rt: humedad del suelo
  if (json_scanf(data, strlen(data), "{ sm:%M }", scan_array_float, 
        pMeasurementStack->soilMoisture) < 0)
    return false;
  // rt: temperatura del suelo
  if (json_scanf(data, strlen(data), "{ st:%M }", scan_array_float, 
        pMeasurementStack->soilTemperature) < 0)
    return false;
  // lu: iluminacion
  if (json_scanf(data, strlen(data), "{ lu:%M }", scan_array_int32_t, 
        pMeasurementStack->luminosity) < 0)
    return false;
  // at: attemps stamps
  if (json_scanf(data, strlen(data), "{ at:%M }", scan_array_int8_t,
        pMeasurementStack->stamp) < 0)
    return false;
  // lt: msglostTime
  if (json_scanf(data, strlen(data), "{ lt:%M }", scan_array_int8_t,
        pMeasurementStack->lostTime) < 0)
    return false;
  /* if (json_scanf(data, strlen(data), "{ rm:%M }", scan_array, my_data) < 0) */
  /*   return false; */
  free(data); // Liberando memoria
  uint8_t i;
  for (i = 1; i < STACK_SIZE; i++)
  {
    if((pMeasurementStack->stamp[i]) != STAMP_MEASUREMENT_EMPTY)
    {
      pMeasurementStack->lostTime[i]--; //retraso relativo para poner en cola
      /* LOG(LL_INFO, ("lostTime: %d", pMeasurementStack->lostTime[i])); */
    }
  }
  return true;
}

bool getDeviceSettings2Var(struct DeviceSettings *pDeviceSetting)
{
  if (!readFile(file_config, &data))
    return false; // No fue exitosa la lectura de los datos
  if( json_scanf(data, strlen(data),
      "{ nn:%Q, np:%Q, m:%Q, mu:%Q, mp:%Q, id:%Q }",
      &pDeviceSetting->networkName,
      &pDeviceSetting->networkPass,
      &pDeviceSetting->mqttUrl,
      &pDeviceSetting->mqttUser,
      &pDeviceSetting->mqttPass,
      &pDeviceSetting->deviceId) < 0 )
    return false;
  free(data);
  return true;
}

//TODO: ésta función debe ser remplazada por el codificador de protobuf nanopb
bool writeStackInFile(struct MeasurementStack *pMeasurementStack)
{
  // TODO: existe problemas al guardar en json los datos del tipo flotante
  // se hace uso de double
  double relativeMoisture[STACK_SIZE];
  uint8_t i;
  for ( i = 0; i < STACK_SIZE; i++)
    relativeMoisture[i] = pMeasurementStack->relativeMoisture[i];
  double relativeTemperature[STACK_SIZE];
  for ( i = 0; i < STACK_SIZE; i++)
    relativeTemperature[i] = pMeasurementStack->relativeTemperature[i];
  double soilMoisture[STACK_SIZE];
  for ( i = 0; i < STACK_SIZE; i++)
    soilMoisture[i] = pMeasurementStack->soilMoisture[i];
  double soilTemperature[STACK_SIZE];
  for ( i = 0; i < STACK_SIZE; i++)
    soilTemperature[i] = pMeasurementStack->soilTemperature[i];
  uint32_t luminosity[STACK_SIZE];
  for ( i = 0; i < STACK_SIZE; i++)
    luminosity[i] = pMeasurementStack->luminosity[i];
  uint8_t attemps[STACK_SIZE];
  for ( i = 0; i < STACK_SIZE; i++)
    attemps[i] = pMeasurementStack->stamp[i];
  int8_t lostTime[STACK_SIZE];
  for ( i = 0; i < STACK_SIZE; i++)
    lostTime[i] = pMeasurementStack->lostTime[i];
  json_fprintf(file_data, "{ rm:%M, rt:%M, sm:%M, st:%M, lu:%M, at:%M, lt:%M }",
      json_printf_array, relativeMoisture,
      sizeof(relativeMoisture), sizeof(relativeMoisture[0]), "%f",
      json_printf_array, relativeTemperature,
      sizeof(relativeTemperature), sizeof(relativeTemperature[0]), "%f",
      json_printf_array, soilMoisture,
      sizeof(soilMoisture), sizeof(soilMoisture[0]), "%f",
      json_printf_array, soilTemperature,
      sizeof(soilTemperature), sizeof(soilTemperature[0]), "%f",
      json_printf_array, luminosity,
      sizeof(luminosity), sizeof(luminosity[0]), "%d",
      json_printf_array, attemps,
      sizeof(attemps), sizeof(attemps[0]), "%d",
      json_printf_array, lostTime,
      sizeof(lostTime), sizeof(lostTime[0]), "%d"
      );
  return true;
}
