#include "handleMqtt.h"

mgos_timer_id timer_id_mqtt_cb;
mgos_timer_id *pTimer_id_mqtt_cb = &timer_id_mqtt_cb;

struct MeasurementStack **pMqttMeasurementStack = NULL;
struct DeviceSettings **pMqttDeviceSetting = NULL;
struct mgos_config_mqtt **ppMqttClient = NULL;

int8_t mqttAttemps = MQTT_ATTEMPS;
int8_t *pMqttAttemps = &mqttAttemps;

bool mqtt_extra = true; // Comodin
bool mqttAttempExtraForPub = true; // Comodin

int8_t mqttItemsSent = -1; // debe empezar en cero al usarse
int8_t stackItem = 0;  // recorrer pila y recorgar ultimo lugar.

char topic[128];

void connect_mqtt_modify_attempts(uint8_t attemps)
{
  mqttAttemps = attemps;
  mqtt_extra = true;
}

bool connect_mqtt_config(
    struct MeasurementStack **dirMeasurementStack,
    struct DeviceSettings **dirDeviceSetting,
    struct mgos_config_mqtt **dirMqttClient
    )
{
  pMqttMeasurementStack = dirMeasurementStack;
  pMqttDeviceSetting = dirDeviceSetting;
  ppMqttClient = dirMqttClient;

  (*ppMqttClient)->enable = true;
  /* (*ppMqttClient)->enable = false; */
  (*ppMqttClient)->server = (*pMqttDeviceSetting)->mqttUrl;
  (*ppMqttClient)->client_id = (*pMqttDeviceSetting)->deviceId;
  /* (*ppMqttClient)->user = ""; */
  (*ppMqttClient)->user = NULL;
  /* (*ppMqttClient)->pass = ""; */
  (*ppMqttClient)->pass = NULL;
  (*ppMqttClient)->reconnect_timeout_min = WIFI_TIMEOUT;
  /* (*ppMqttClient)->reconnect_timeout_max = WIFI_TIMEOUT*10; */
  (*ppMqttClient)->reconnect_timeout_max = WIFI_TIMEOUT;
  /* (*ppMqttClient)->ssl_cert = ""; */
  /* (*ppMqttClient)->ssl_key = ""; */
  /* (*ppMqttClient)->ssl_ca_cert = ""; */
  /* (*ppMqttClient)->ssl_cipher_suites = ""; */
  /* (*ppMqttClient)->ssl_psk_identity = ""; */
  /* (*ppMqttClient)->ssl_psk_key = ""; */
  (*ppMqttClient)->clean_session = true;
  (*ppMqttClient)->keep_alive = 60;
  /* (*ppMqttClient)->will_topic = ""; */
  (*ppMqttClient)->will_topic = NULL;
  /* (*ppMqttClient)->will_message = ""; */
  (*ppMqttClient)->will_message = NULL;
  (*ppMqttClient)->will_retain = false;
  (*ppMqttClient)->max_qos = 2;
  (*ppMqttClient)->recv_mbuf_limit = 3072;
  (*ppMqttClient)->require_time = false;
  (*ppMqttClient)->cloud_events = true;
  (*ppMqttClient)->max_queue_length = 5;
  /* (*ppMqttClient)->will_message = ""; */
  (*ppMqttClient)->will_message = NULL;
  /* (*ppMqttClient)->will_topic = ""; */
  (*ppMqttClient)->will_topic = NULL;

  /* strcpy(topic, "SEMVA/"); */
  /* strcpy(topic, "SEMVASENS"); */
  /* mas de dos string */
  /* sprintf(topic, "%s%s%s", "SEMVA/", (*ppMqttClient)->client_id, "/SEMVASENS"); */
  sprintf(topic, "%s%s", (*ppMqttClient)->client_id, "/SEMVASENS");
  mgos_mqtt_set_config(*ppMqttClient);
  return true;
}

uint8_t auxAttemps = 0;
static void mqtt_connecting_cb(void *arg)
{
  auxAttemps++;
  if(mgos_mqtt_global_is_connected() == true)
  {
    LOG(LL_INFO, ("CONNECTADO A MQTT GLOBAL"));
    mgos_clear_timer(*pTimer_id_mqtt_cb);
    /* LOG(LL_INFO, ("conectado mqtt")); */
    mqtt_public_semva_and_update_stack();
  }else
  {
    LOG(LL_INFO, ("Attempes aux %d", auxAttemps));
    if (mqttAttemps > 0)
    {
      if (auxAttemps == 10)
      {
        auxAttemps = 0;
        mqttAttemps--;
        /* LOG(LL_INFO, ("MQTT esta conectado el mqtt? %d", mgos_mqtt_global_is_connected())); */
        /* LOG(LL_INFO, ("MQTT get global conenection...: %d", (mgos_mqtt_get_global_conn() != NULL))); */
        if(mgos_wifi_get_status() != MGOS_WIFI_IP_ACQUIRED && mqtt_extra == true)
        {
          LOG(LL_INFO, ("Se ha desconectado de WIFI reintentar una vez mas desde mqtt"));
          mqtt_extra = false;
          connect_wifi_modify_attempts(1);
          connect_mqtt_modify_attempts(3);
        }else
        {
          /* mgos_mqtt_global_disconnect(); */
          /* mgos_mqtt_global_connect(); */
          LOG(LL_INFO, ("Intento de conexion restantes: %d", mqttAttemps));
        }
      }
    } else
    {
      mgos_clear_timer(*pTimer_id_mqtt_cb);
      // TODO La función no está cumpliendo con su cometido requiere ser REVISADO
      mgos_mqtt_global_disconnect();
      LOG(LL_INFO, ("Todos los intentos de MQTT agotados"));
      (*pMqttMeasurementStack)->stamp[0] = STAMP_MQTT_MISSING;
      writeStackInFile(*pMqttMeasurementStack);
      semva_sleep();
    }
  }
  (void) arg;
}

void connect_mqtt()
{
  auxAttemps = 0;
  /* if(mgos_mqtt_set_config(*ppMqttClient)) */
  /*   return true; */
  /* mgos_mqtt_global_disconnect(); */
  /* mgos_mqtt_set_config(*ppMqttClient); */
  /* if(mgos_mqtt_set_config(*ppMqttClient)){ */
    /* if(mgos_mqtt_global_connect()) */
    /* { */
  /* mgos_mqtt_set_config(*ppMqttClient); */
      /* mgos_mqtt_global_disconnect(); */
  mgos_mqtt_global_connect();
  *pTimer_id_mqtt_cb = mgos_set_timer(500, MGOS_TIMER_REPEAT, 
    mqtt_connecting_cb, NULL);
    /* } */
  /* } */
      /* mqtt_public_semva_and_update_stack(); */
}

bool array_update_to_save(void)
{
  //mqttItemsSent es el último lugar de la pila que pudo enviar
  uint8_t stackCounter1 = 0;
  uint8_t stackCounter2;
  if (mqttItemsSent == 0 && (*pMqttMeasurementStack)->stamp[1]== STAMP_MEASUREMENT_EMPTY )
  {
    return false; // No actualizado por tanto no requiere ser guardado en memoria flash
  }else if (mqttItemsSent == -1)
  {
    return true; // la pila debe ser guardada ya que nada pudo ser almacenado
  }else if (mqttItemsSent < 9)
  {
    for (stackCounter1 = 0; stackCounter1 < STACK_SIZE; stackCounter1++)
    {
      if ((*pMqttMeasurementStack)->stamp[mqttItemsSent + 1] != STAMP_MEASUREMENT_EMPTY )
      {
        // al llenar la pila para almacenar empieza en 0 corriendo los datos desde el que no pudi ser almacenado
        (*pMqttMeasurementStack)->stamp[stackCounter1] = (*pMqttMeasurementStack)->stamp[mqttItemsSent + 1];
        mqttItemsSent++;
      }else{
        break;
      }
    }
  }
  for (stackCounter2 = stackCounter1; stackCounter2 < STACK_SIZE; stackCounter2++)
  {
    (*pMqttMeasurementStack)->stamp[stackCounter2] = STAMP_MEASUREMENT_EMPTY;
    (*pMqttMeasurementStack)->lostTime[stackCounter2] = 0;
    (*pMqttMeasurementStack)->relativeMoisture[stackCounter2] = 0.0;
    (*pMqttMeasurementStack)->relativeTemperature[stackCounter2] = 0.0;
    (*pMqttMeasurementStack)->soilMoisture[stackCounter2] = 0.0;
    (*pMqttMeasurementStack)->soilTemperature[stackCounter2] = 0.0;
    (*pMqttMeasurementStack)->luminosity[stackCounter2] = 0;
  }
  return true;
} 


/* pb_SEMVAMVP semvaPayload = pb_SEMVAMVP_init_zero; */

static void mqtt_public_semva_and_update_stack_cb(void *arg)
{
  /* if (mgos_mqtt_global_is_connected() == true) */
  /* LOG(LL_INFO, ("MQTT esta conectado el mqtt? %d", mgos_mqtt_global_is_connected())); */
  /* LOG(LL_INFO, ("MQTT enviando mensaje %d", */
  /*       mgos_mqtt_pub("sensor/saludo", "hi", 2, 1, 0) */
  /*       )); */

  stackItem = mqttItemsSent + 1;
  if((*pMqttMeasurementStack)->stamp[stackItem] != STAMP_MEASUREMENT_EMPTY && stackItem < 10)
  {
    LOG(LL_INFO, ("Send message mqttt %d", stackItem));
    pb_SEMVAMVP semvaPayload = pb_SEMVAMVP_init_zero;
    semvaPayload.msgLostErrorCode = (*pMqttMeasurementStack)->stamp[stackItem];
    semvaPayload.msgLostTime = (*pMqttMeasurementStack)->lostTime[stackItem];
    semvaPayload.humidityAir = (*pMqttMeasurementStack)->relativeMoisture[stackItem];
    semvaPayload.temperatureAir = (*pMqttMeasurementStack)->relativeTemperature[stackItem];
    semvaPayload.humiditySoil = (*pMqttMeasurementStack)->soilMoisture[stackItem];
    semvaPayload.temperatureSoil = (*pMqttMeasurementStack)->soilTemperature[stackItem];
    semvaPayload.light = (*pMqttMeasurementStack)->luminosity[stackItem];

    uint8_t buffer[128];
    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
    bool status = pb_encode(&stream, pb_SEMVAMVP_fields, &semvaPayload);
    char nanoobPayload [stream.bytes_written];

    memcpy(nanoobPayload, buffer, stream.bytes_written);
    if (status)
    {
      if(
          /* mgos_mqtt_pub("sensor/saludo", nanoobPayload, (uint8_t)stream.bytes_written, 1, 0) > 0 */
          mgos_mqtt_pub(topic, nanoobPayload, (uint8_t)stream.bytes_written, 1, 0) > 0
        )
      {
        mqttItemsSent++; // actualizar al siguiente valor de pila a enviar
      }else
      {
        if(mgos_mqtt_global_is_connected() == false && mqttAttempExtraForPub)
        {
          mqttAttempExtraForPub = false;
          connect_mqtt_modify_attempts(1); // Intento adicional por desconexión
        }else
        {
          mgos_clear_timer(*pTimer_id_mqtt_cb);
          LOG(LL_INFO, ("No envio todo y se prepara para guardar"));
          if(array_update_to_save())
            writeStackInFile(*pMqttMeasurementStack);
          semva_sleep();
        }
      }
    }else
    {
      LOG(LL_INFO, ("Failed to encode"));
    }
  }else
  {
    mgos_clear_timer(*pTimer_id_mqtt_cb);
    LOG(LL_INFO, ("Finish to send mqtt"));
    if(array_update_to_save())
      writeStackInFile(*pMqttMeasurementStack);
    semva_sleep();
  }
  (void) arg;
}

/* //MANEJADOR DE EVENTOS SUB MQTT */
/* static void handler(struct mg_connection *c, const char *topic, int topic_len, */
/*                     const char *msg, int msg_len, void *userdata) { */
/*   LOG(LL_INFO, ("Got message on topic %.*s", topic_len, topic)); */
/* } */


void mqtt_public_semva_and_update_stack()
{
  *pTimer_id_mqtt_cb = mgos_set_timer(1000, MGOS_TIMER_REPEAT, 
    mqtt_public_semva_and_update_stack_cb, NULL);


  /* LOG(LL_INFO, ("Here")); */
  /* semvaPayload.msgLost = (*pMqttMeasurementStack)->stamp[0]; */
  /* semvaPayload.humidityAir = (*pMqttMeasurementStack)->relativeMoisture[0]; */
  /* semvaPayload.temperatureAir = (*pMqttMeasurementStack)->relativeTemperature[0]; */
  /* semvaPayload.humiditySoil = (*pMqttMeasurementStack)->soilMoisture[0]; */
  /* semvaPayload.temperatureSoil = (*pMqttMeasurementStack)->soilTemperature[0]; */
  /* semvaPayload.light = (*pMqttMeasurementStack)->luminosity[0]; */

  /* uint8_t buffer[128]; */
  /* pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer)); */
  /* bool status = pb_encode(&stream, pb_SEMVAMVP_fields, &semvaPayload); */

  /* LOG(LL_INFO, ("Iniciar nanoobPayload")); */
  /* char nanoobPayload [stream.bytes_written]; */
  /* uint8_t i = 0; */
  /* for ( i = 0; i<stream.bytes_written; i++) */
  /* { */
  /*   if(sprintf(nanoobPayload, "%d", buffer[i])){ */
  /*     LOG(LL_INFO, ("%x", buffer[i])); */
  /*   } */
  /* } */

  /* char* nanoobPayload[]; */
  /* memcpy(nanoobPayload, buffer, stream.bytes_written); */
  /* if (!status) */
  /* { */
  /*   LOG(LL_INFO, ("Failed to encode")); */
  /* }else */
  /* { */

    /* // TODO: modelo para sub en MQTT */ 
    /* mgos_mqtt_sub("sensor/#", handler, NULL);       /1* Subscribe *1/ */

    /* LOG(LL_INFO, ("Logitud mensaje %d", (uint8_t)stream.bytes_written)); */
    /* LOG(LL_INFO, ("enviando mensaje %d", */
    /*       mgos_mqtt_pub("sensor/saludo", nanoobPayload, (uint8_t)stream.bytes_written, 1, 0) */
    /*       )); */
    /* free(nanoobPayload); */
    /* char* nanoobPayload = &buffer; */
    /* LOG(LL_INFO, ("Dato a enviar %s", nanoobPayload)); */
    /* free(nanoobPayload); */
    /* char* topic ="sensor/1"; */
    /* char* payload = "message"; */
    /* LOG(LL_INFO, ("enviando mensaje %d", */
    /*       mgos_mqtt_pub("sensor/saludo", "hi", 2, 1, 0) */
    /*       )); */
    /* LOG(LL_INFO, ("enviando mensaje %d", */
    /*       mgos_mqtt_pub("sensor/saludo", "hi", 2, 1, 0) */
    /*       )); */
    /* LOG(LL_INFO, ("enviando mensaje %d", */
    /*       mgos_mqtt_pub("sensor/saludo", "hi", 2, 1, 0) */
    /*       )); */
  /* } */
  /* *pTimer_id_mqtt_cb = mgos_set_timer(1000, MGOS_TIMER_REPEAT, */ 
  /*   mqtt_test_cb, NULL); */
}

