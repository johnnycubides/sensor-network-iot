/**
 * @file 
 * @brief representación de la lectura de todos los sensores
 * @author Johnny Cubides
 * contact <jgcubidesc@gmail.com>
 * @date Wednesday 25 March 2020
 **/

#include "sensor_reading.h"

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);
Adafruit_seesaw ss;
sensor_t sensor;

/* mgos_timer_id timer_id_sensors_cb; */
/* mgos_timer_id *pTimer_id_sensors_cb = &timer_id_sensors_cb; */

struct mgos_i2c *i2c;
static struct mgos_si7021 *s_si7021; /*!< Estructura que representa al sensor si7021 */

/* Measurements measurements; */

/* INICIAR TODOS LOS SENSORES */

void initSensors(struct MeasurementStack *pMeasurementStack)
{
  /* measurements.luminosity = 3; */
  /* LOG(LL_INFO, ("initSensors")); */
  /* mos_measurements = &measurements; */
  /* LOG(LL_INFO, ("iluminacion inicial: %d", measurements.luminosity)); */
  /* LOG(LL_INFO, ("iluminacion inicial: %d", mos_measurements->luminosity)); */
  if(!initTsl())
  {
    pMeasurementStack->luminosity[0] = NOTFOUND;
    pMeasurementStack->stamp[0] = STAMP_SENSOR_MISSING;
    LOG(LL_INFO, ("tsl not found"));
  }
  if (!initSeeSaw())
  {
    pMeasurementStack->soilMoisture[0] = NOTFOUND;
    pMeasurementStack->soilTemperature[0] = NOTFOUND;
    pMeasurementStack->stamp[0] = STAMP_SENSOR_MISSING; 
    LOG(LL_INFO, ("SeeSaw not found"));
  }
  if (!initSi7021())
  {
    pMeasurementStack->relativeMoisture[0] = NOTFOUND;
    pMeasurementStack->relativeTemperature[0] = NOTFOUND;
    pMeasurementStack->stamp[0] = STAMP_SENSOR_MISSING; 
    LOG(LL_INFO, ("si7021 not found"));
  }
}




void readSensors(struct MeasurementStack *pMeasurementStack)
{
  uint8_t* stamp = &pMeasurementStack->stamp[0];
  float* relativeMoisture = &pMeasurementStack->relativeMoisture[0];
  float* relativeTemperature = &pMeasurementStack->relativeTemperature[0];
  float* soilMoisture = &pMeasurementStack->soilMoisture[0];
  float* soilTemperature = &pMeasurementStack->soilTemperature[0];
  int32_t* luminosity = &pMeasurementStack->luminosity[0];
  // TODO devido a que se preparan los sensores para su trabajo
  // en esa lectura inicial puede generar errores de valores válidos
  // así que se  requiere que si da un valor no valido, al llamar ésta función
  // nuevamente debe limpiarse el stamp si éste representa un valor no valido
  if(*stamp == STAMP_MEASUREMENT_FAIL)
    *stamp = STAMP_MEASUREMENT_EMPTY;
  /* if(measurements.luminosity != (int)NOTFOUND) */
  if(((int8_t)(*luminosity)) != NOTFOUND)
  {
    uint8_t i;
    for (i = 0; i < ATTEMPS; i++) {
      *luminosity = tsl.getLuminosity(TSL2591_VISIBLE);    
      if ( *luminosity <= MIN_LUMINOSITY ||
          *luminosity >= MAX_LUMINOSITY)
      {
        *luminosity = INVALID_VALUE;
        if (*stamp != STAMP_SENSOR_MISSING && i == (ATTEMPS - 1))
          *stamp = STAMP_MEASUREMENT_FAIL;
      } else
      {
        if (*stamp != STAMP_SENSOR_MISSING && *stamp != STAMP_MEASUREMENT_FAIL)
          *stamp = STAMP_SENSOR_OK;
        break;
      }
    }
    // TODO
    /* measurements.luminosity = VALID_VALUE(MIN_LUMINOSITY, tsl.getLuminosity(TSL2591_VISIBLE), MAX_LUMINOSITY); */
  }
  if(((int8_t)(*soilTemperature)) != NOTFOUND)
  {
    uint8_t i;
    bool okSoilMoisture = false;
    bool okSoilTemperature = false;
    for (i = 0; i < ATTEMPS; i++)
    {
      *soilMoisture = ss.touchRead(0)/10.0;    
      *soilTemperature = ss.getTemp();
      if ( *soilMoisture <= MIN_SOIL_MOISTURE ||
          *soilMoisture >= MAX_SOIL_MOISTURE)
      {
        *soilMoisture = INVALID_VALUE;
        okSoilMoisture = false;
      } else
        okSoilMoisture = true;
      if (*soilTemperature <= MIN_SOIL_TEMPERATURE ||
          *soilTemperature >= MAX_SOIL_TEMPERATURE)
      {
        *soilTemperature = INVALID_VALUE;
        okSoilTemperature = false;
      } else
        okSoilTemperature = true;
      if (okSoilMoisture == true && okSoilTemperature == true)
      {
        if (*stamp != STAMP_SENSOR_MISSING && *stamp != STAMP_MEASUREMENT_FAIL)
          *stamp = STAMP_SENSOR_OK;
        break;
      }else
      {
        if (*stamp != STAMP_SENSOR_MISSING && i == (ATTEMPS - 1))
          *stamp = STAMP_MEASUREMENT_FAIL;
      }
    }
  }
  if(((int8_t)(*relativeTemperature)) != NOTFOUND)
  {
    uint8_t i;
    bool okRelativeMoisture = false;
    bool okRelativeTemperature = false;
    //TODO éste sensor requiere un tiempo para prepararse,
    //para los anteriores basados en arduino resuelven el problema 
    //a partir de arduino compact. para el uso correcto de mos se
    //requiere hacer uso de eventos, por tanto se requiere user otro
    //metodo para el temporizador, en éste caso para el for
    //  SE REQUIERE RESTRUCTURAR ÉSTA PARTE COMPLETAMENTE
    //  PARA EVITAR EL TIMER EN EL MAIN DE 5SEGUNDOS.
    for (i = 0; i < ATTEMPS; i++)
    {
      /* if(mgos_si7021_read(s_si7021)){ */
        /* LOG(LL_INFO, ("read sensor si7021")); */
      *relativeMoisture = mgos_si7021_getHumidity(s_si7021);
      *relativeTemperature = mgos_si7021_getTemperature(s_si7021);
        /* LOG(LL_INFO, ("temp %f", mgos_si7021_getTemperature(s_si7021))); */
      /* } */
      if ( *relativeMoisture <= MIN_RELATIVE_MOISTURE ||
          *relativeMoisture >= MAX_RELATIVE_MOISTURE)
      {
        *relativeMoisture = INVALID_VALUE;
        okRelativeMoisture = false;
      } else
        okRelativeMoisture = true;
      if (*relativeTemperature <= MIN_RELATIVE_TEMPERATURE ||
          *relativeTemperature >= MAX_RELATIVE_TEMPERATURE)
      {
        *relativeTemperature = INVALID_VALUE;
        okRelativeTemperature = false;
      } else
        okRelativeTemperature = true;
      if (okRelativeMoisture == true && okRelativeTemperature == true)
      {
        if (*stamp != STAMP_SENSOR_MISSING && *stamp != STAMP_MEASUREMENT_FAIL)
          *stamp = STAMP_SENSOR_OK;
        break;
      }else
      {
        if (*stamp != STAMP_SENSOR_MISSING && i == (ATTEMPS - 1))
          *stamp = STAMP_MEASUREMENT_FAIL;
      }
    }
  }
}

/* SENSOR DE ILUMINACIÓN */
bool initSeeSaw(void)
{
  for (int i = 0; i < ATTEMPS; i++)
  {
    /* LOG(LL_INFO, ("initTsl")); */
    if(ss.begin(0x36))
    {
      /* LOG(LL_INFO, ("initTsl begin")); */
      return true;
    }
  }
  return false;
}

/* INICIO DE SENSOR DE ILUMINACIÓN */
bool initTsl(void)
{
  for (int i = 0; i < ATTEMPS; i++)
  {
    /* LOG(LL_INFO, ("initTsl")); */
    if(tsl.begin())
    {
      /* LOG(LL_INFO, ("initTsl begin")); */
      tsl.getSensor(&sensor);
      cfgSenTsl();
      return true;
    }
  }
  return false;
}

void cfgSenTsl(void)
{
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  //tsl.setGain(TSL2592_GAIN_LOW);    // 1x gain (bright light)
  tsl.setGain(TSL2591_GAIN_MED);      // 25x gain
  //tsl.setGain(TSL2591_GAIN_HIGH);   // 428x gain
  
  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  //tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS);  // shortest integration time (bright light)
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS);  // longest integration time (dim light)

  /* Display the gain and integration time for reference sake */  
  /* Serial.println(F("------------------------------------")); */
  /* Serial.print  (F("Gain:         ")); */
  tsl2591Gain_t gain = tsl.getGain();
  switch(gain)
  {
    case TSL2591_GAIN_LOW:
      LOG(LL_INFO, ("1x (Low)"));
      break;
    case TSL2591_GAIN_MED:
      LOG(LL_INFO, ("25x (Medium)"));
      break;
    case TSL2591_GAIN_HIGH:
      LOG(LL_INFO, ("428x (High)"));
      break;
    case TSL2591_GAIN_MAX:
      LOG(LL_INFO, ("9876x (Max)"));
      break;
  }
  LOG(LL_INFO, ("Timing:       "));
  LOG(LL_INFO, ("%d", (tsl.getTiming() + 1) * 100));
  LOG(LL_INFO, (" ms"));
  LOG(LL_INFO, ("------------------------------------"));
}

bool initSi7021(void)
{
  for (int i = 0; i < ATTEMPS; i++)
  {
    i2c=mgos_i2c_get_global();
    if(i2c)
    {
      s_si7021=mgos_si7021_create(i2c, 0x40);
      if(s_si7021)
        return true;
    }
  }
  return false;
}
