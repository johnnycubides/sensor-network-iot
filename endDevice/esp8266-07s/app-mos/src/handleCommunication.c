#include "handleCommunication.h"

mgos_timer_id timer_id_wifi_cb;
mgos_timer_id *pTimer_id_wifi_cb = &timer_id_wifi_cb;

struct MeasurementStack **ppMeasurementStack = NULL;
struct DeviceSettings **ppDeviceSetting = NULL;
struct mgos_config_wifi_sta **ppStation = NULL;

int8_t wifiAttemps = WIFI_ATTEMPS;
int8_t *pWifiAttemps = &wifiAttemps;

void connect_wifi_modify_attempts(uint8_t attemps)
{
  wifiAttemps = attemps;
}

bool connect_wifi_config(
    struct MeasurementStack **dirMeasurementStack,
    struct DeviceSettings **dirDeviceSetting,
    struct mgos_config_wifi_sta **dirStation)
{
  ppMeasurementStack = dirMeasurementStack;
  ppDeviceSetting = dirDeviceSetting;
  ppStation = dirStation;
  (*ppStation)->ssid = (*ppDeviceSetting)->networkName;
  (*ppStation)->pass = (*ppDeviceSetting)->networkPass;
  (*ppStation)->enable = true;
  char *msg = NULL;
  char **pmsg = &msg;
  if(mgos_wifi_validate_sta_cfg(*ppStation, pmsg))
  {
    free(msg);
    return true;
  }else
  {
    LOG(LL_INFO, ("cfg wifi bad: %s", msg));
    free(msg);
    return false;
  }
}

static void wifi_connecting_cb(void *arg)
{
  /* enum mgos_wifi_status { */
  /*   MGOS_WIFI_DISCONNECTED = 0, */
  /*   MGOS_WIFI_CONNECTING = 1, */
  /*   MGOS_WIFI_CONNECTED = 2, */
  /*   MGOS_WIFI_IP_ACQUIRED = 3, */
  /* }; */
  enum mgos_wifi_status status_conection = mgos_wifi_get_status();
  LOG(LL_INFO, ("estado de la conexion %d", status_conection));
  if(status_conection == MGOS_WIFI_IP_ACQUIRED)
  {
    LOG(LL_INFO, ("condicion dada"));
    mgos_clear_timer(*pTimer_id_wifi_cb);
    LOG(LL_INFO, ("Inicio de proceso conexion mqtt"));
    connect_mqtt();
  }else if (status_conection == MGOS_WIFI_DISCONNECTED)
  {
    mgos_clear_timer(*pTimer_id_wifi_cb);
    LOG(LL_INFO, ("Desconectado, intentos restantes: %d", wifiAttemps));
    connect_wifi();
  }
  (void) arg;
}

void connect_wifi()
{
  mgos_wifi_disconnect();
  if(wifiAttemps >=1)
  {
    wifiAttemps--;
    /* cfg->ssid = "UNE_HFC_EA1F"; */
    /* cfg->pass = "XQIDC9HE"; */
    /* cfg->enable = true; // TODO*/
    if(mgos_wifi_setup_sta(*ppStation)) //iniciar intento conexion
    {
      *pTimer_id_wifi_cb = mgos_set_timer(1000, MGOS_TIMER_REPEAT, 
          wifi_connecting_cb, NULL);
    }
    else
    {
      // Problema lógico, no de intentos de conexión
      LOG(LL_INFO, ("No fue posible configurar la sta para conectar")); 
    }
  } else
  {
    LOG(LL_INFO,("Se hicieron todos los intentos posibles"));
    (*ppMeasurementStack)->stamp[0] = STAMP_WIFI_MISSING;
    mgos_wifi_deinit();
    writeStackInFile(*ppMeasurementStack);
    semva_sleep();
  }
}

