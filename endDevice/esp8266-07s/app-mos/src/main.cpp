/**
 * @file 
 * @brief Aplicación principal
 * @author Johnny Cubides
 * contact <jgcubidesc@gmail.com>
 * @date Wednesday 25 March 2020
 **/

#include "main.h"

/* struct Measurements measurements; */
/* struct Measurements *mos_measurements = &measurements; */
/* mos_measurements = &measurements; */

struct MeasurementStack measurementStack;
struct MeasurementStack *pMeasurementStack = &measurementStack;

struct DeviceSettings deviceSetting;
struct DeviceSettings *pDeviceSetting = &deviceSetting;

struct mgos_config_wifi_sta station;
struct mgos_config_wifi_sta *pStation = &station;


struct mgos_config_mqtt mqttClient;
struct mgos_config_mqtt *pMqttClient = &mqttClient;

mgos_timer_id timer_id_sensors_ready_cb;
mgos_timer_id *pTimer_id_sensors_ready_cb = &timer_id_sensors_ready_cb;

/* static void timer_cb(void *arg) */
/* { */
  /* static bool s_tick_tock = false; */
  /* LOG(LL_INFO, */
  /*     ("%s uptime: %.2lf, RAM: %lu, %lu free", (s_tick_tock ? "Tick" : "Tock"), */
  /*      mgos_uptime(), (unsigned long) mgos_get_heap_size(), */
  /*      (unsigned long) mgos_get_free_heap_size())); */
  /* readSensors(mos_measurements); */
  /* LOG(LL_INFO, ("%d",mos_measurements->luminosity)); */
  /* LOG(LL_INFO, ("%f",mos_measurements->soilMoisture)); */
  /* LOG(LL_INFO, ("%f",mos_measurements->soilTemperature)); */
  /* s_tick_tock = !s_tick_tock; */
/* #ifdef USER_LED */
  /* mgos_gpio_toggle(USER_LED); */
/* #endif */
  /* readSensors(pMeasurementStack); */
  /* LOG(LL_INFO,("stamp: %d", measurementStack.stamp[0])); */
  /* LOG(LL_INFO,("soilMoisture: %f", measurementStack.soilMoisture[0])); */
  /* LOG(LL_INFO,("soilTemperature: %f", measurementStack.soilTemperature[0])); */
  /* LOG(LL_INFO,("relativeMoisture: %f", measurementStack.relativeMoisture[0])); */
  /* LOG(LL_INFO,("relativeTemperature: %f", measurementStack.relativeTemperature[0])); */
  /* LOG(LL_INFO,("luminosity: %d", measurementStack.luminosity[0])); */

  /* LOG(LL_INFO, ("Reporte de memoria: RAM: %lu total, %lu free, %lu min free", */
  /*               (unsigned long) mgos_get_heap_size(), */
  /*               (unsigned long) mgos_get_free_heap_size(), */
  /*               (unsigned long) mgos_get_min_free_heap_size())); */
  /* LOG(LL_INFO, ("status: %d", mgos_wifi_get_status())); */
  /* (void) arg; */
/* } */

static void timer_cb(void *arg)
{
  mgos_clear_timer(*pTimer_id_sensors_ready_cb);
  readSensors(pMeasurementStack);
  if(connect_wifi_config(&pMeasurementStack, &pDeviceSetting, &pStation))
  {
    if(connect_mqtt_config(&pMeasurementStack, &pDeviceSetting, &pMqttClient))
    {
      connect_wifi();
    }
  }
  (void) arg;
}

enum mgos_app_init_result mgos_app_init(void)
{
  semva_setup_gpio();
  LOG(LL_INFO, ("init principal"));

  if (!getMeasurement2Stack(pMeasurementStack))
    LOG(LL_INFO,("Fail getMeasurement2Stack()"));
  if (!getDeviceSettings2Var(pDeviceSetting))
    LOG(LL_INFO,("Fail getDeviceSettings2Var()"));
  /* LOG(LL_INFO, ("configure: %s", deviceSetting.networkName)); */
  /* LOG(LL_INFO, ("pass %s", pDeviceSetting->networkName)); */
  initSensors(pMeasurementStack);
  readSensors(pMeasurementStack); // preparando sensores


  /* if(connectWifi(pStation, WIFI_ATTEMPS, */
  /*       pDeviceSetting, pMeasurementStack)) */
  /*   LOG(LL_INFO, ("connect wifi")); */
  /* else */ 
  /*   LOG(LL_INFO, ("NO connect wifi")); */

  /* initSensors(pMeasurementStack); */
  *pTimer_id_sensors_ready_cb =  mgos_set_timer(5000 /* ms */,
      MGOS_TIMER_REPEAT, timer_cb, NULL);

  return MGOS_APP_INIT_SUCCESS;
} 
