/**
 * @file 
 * @brief definiciones de elementos para semva
 * @author Johnny Cubides
 * contact jgcubidesc@gmail.com
 * @date Sunday 12 April 2020
 **/

#ifndef SEMVA_H
#define SEMVA_H
#include <stdint.h>
#include "mgos.h"

#define STACK_SIZE 10 /*!< Tamaño de pila de mediciones */
#define WIFI_ATTEMPS 3 /*!< Número de intentos para conectarse a wifi */
#define WIFI_TIMEOUT 5 /*!< Tiempo de cada intento */
#define MQTT_ATTEMPS 3 /*!< Número de intentos para conectarse al broker */
#define MQTT_TIMEOUT 5 /*!< Tiempo de cada intento al tratar de conectarse a MQTT */

#ifndef USER_LED
#define USER_LED 13
#endif /* ifndef USER_LED */
#ifndef PIN_DONE
#define PIN_DONE 14
#endif /* ifndef PIN_DONE */


#ifdef __cplusplus
extern "C" {
#endif

/**
 *	Pila de mediciones
 */
struct MeasurementStack
{
  uint8_t stamp[STACK_SIZE]; /*!< Registro de sucesos en las mediciones */
  int8_t lostTime[STACK_SIZE]; /*!< Registro de datos en cola por no ser enviado */
  float relativeMoisture[STACK_SIZE]; /*!< Registro de la humedad relativa */
  float relativeTemperature[STACK_SIZE]; /*!< Registro de la temperatura relativa */
  float soilMoisture[STACK_SIZE]; /*!< Registro de la humedad del suelo */
  float soilTemperature[STACK_SIZE]; /*!< Registro de la temperatura del suelo */
  int32_t luminosity[STACK_SIZE]; /*!< Registro de valores de iluminación */
};

/**
 *	Estructura que representa la configuración de dispositivo
 *	tanto para conectarse como para identificarse
 */
struct DeviceSettings
{
  char* networkName; /*!< Nombre de red */
  char* networkPass; /*!< Contraseña de red */
  char* mqttUrl; /*!< Dirección del broker */
  char* mqttUser; /*!< Usuario MQTT */
  char* mqttPass; /*!< Contraseña MQTT */
  char* deviceId; /*!< identificación de dispositivo */
};

/**
 *	Posibles respuestas para los eventos de mediciones
 */
enum StampResponse
{
  STAMP_MEASUREMENT_EMPTY=0, /*!< El espacio de pila de ésta medición está vacio */
  STAMP_SENSOR_OK=1, /*!< La medición de sensores no reportó ningún error */
  STAMP_SENSOR_MISSING=2, /*!< Un sensor o vairios sensores no fueron localizados */
  STAMP_MEASUREMENT_FAIL=3, /*!< Un sensor ha reportado un valor fuera del rango */
  STAMP_WIFI_MISSING=4, /*!< No se alcanzó la conexión wifi */
  STAMP_MQTT_MISSING=5, /*!< No se alcanzó broker */
};

/**
 * @brief Poner en modo salida el pin DONE y el LED de usuario
 **/
void semva_setup_gpio(void);

/**
 * @brief Terminada las operaciones indicará que se debe realizar un apagado
 * por hardware
 **/
void semva_sleep(void);

/**
 * @brief Intercambia los valores del LED
 **/
void user_led_toggle(void);

/**
 * @brief Pone el LED de usario a 1
 **/
void user_led_on(void);

/**
 * @brief Pone el LED de usuario a 0
 **/
void user_led_off(void);


#ifdef __cplusplus
}
#endif
#endif /* ifndef SEMVA_H */
