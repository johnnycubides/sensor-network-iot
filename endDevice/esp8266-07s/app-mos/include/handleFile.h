/**
 * @file 
 * @brief permite controlar el almacenamiento y lectura de los datos de sensores como de configuración
 * @author Johnny Cubides
 * contact jgcubidesc@gmail.com
 * @date Saturday 11 April 2020
 **/

#ifndef HANDLEFILE_H
#define HANDLEFILE_H

#include "mgos.h"
#include "frozen.h"
#include "semva.h"

#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STACK_SIZE 10 /*!< Éste valor corresponde al tamaño de la pila para recorrerla */
#define ATTEMPS_READ_FILE 2 /*!< Intentos para leer archivo */

#ifdef __cplusplus
extern "C" {
#endif

char *data; /*!< Apuntador para captura de datos desde el jsonfile, después de usar free(data) */

/**
 * @brief Lee los datos de un archivo almacenado en la memoria flash
 * @param nombre del archivo
 * @param lugar donde se almacenan los datos
 * @return verdadero si los datos fueron cargados, falso si falla esa operación
 **/
bool readFile(const char* filename, char ** data);

/**
 * @brief Escribe los datos a archivo en la memoria flash
 * @param Variables de donde provienen los almacenan
 * @return verdadero si los datos fueron guardados, falso si falla esa operación
 **/
bool writeStackInFile(struct MeasurementStack *pMeasurementStack);

/**
 * @brief Carga los valores almacenados en un archivo a la pila
 * @param Structura de la pila de mediciones
 * @return true si se obtienen los datos de la pila
 **/
bool getMeasurement2Stack(struct MeasurementStack *pMeasurementStack);

/**
 * @brief Obtener la configuración para comunicación con otros
 * dispositovos como su configuración
 * @param pDeviceSetting puntero que se refiere a las
 * variables donde se guardarán los datos de configuración
 * @return retorna si es exitosa la escritura de la configuración
 * retornara true
 **/
bool getDeviceSettings2Var(struct DeviceSettings *pDeviceSetting);


#ifdef __cplusplus
}
#endif
#endif /* ifndef HANDLEFILE_H */
