/**
 * @file 
 * @brief Aplicación principal
 * @author Johnny Cubides
 * contact <jgcubidesc@gmail.com>
 * @date Wednesday 25 March 2020
 **/

#ifndef MAIN_H
#define MAIN_H
#include "mgos_i2c.h"
#include "mgos.h"
#include "semva.h"
#include "handleFile.h"
#include "handleCommunication.h"
#include "handleMqtt.h"
#include "sensor_reading.h"



/* const struct mgos_config_i2c cfg = { */
/*   .enable = true, */
/*   .freq = 400, */
/*   .debug = 0, */
/*   .sda_gpio = 4, */
/*   .scl_gpio = 5 */
/* }; */

#endif /* ifndef MAIN_H */ 
