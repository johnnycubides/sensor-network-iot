/**
 * @file 
 * @brief Funciones para comunicación inalambrica y
 * protocolo de comunicación MQTT
 * @author Johnny Cubides
 * contact jgcubidesc@gmail.com
 * @date Monday 13 April 2020
 **/

#ifndef HANDLECOMMUNICATION_H
#define HANDLECOMMUNICATION_H


#include <stdint.h>
#include "semva.h"
#include "mgos.h"
#include "mgos_wifi.h"
#include "handleMqtt.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Conectar a WiFi red
 **/
void connect_wifi();

/**
 * @brief Configurar red antes de conectar
 * @param **dirMeasurementStack actualizar estado si falla conexión wifi
 * @param **dirDeviceSetting parámetros de configuración
 * @param **dirStation apuntador a configuración wifi a usar y monitorear
 * @return Validar la configuración
 **/
bool connect_wifi_config(
    struct MeasurementStack **dirMeasurementStack,
    struct DeviceSettings **dirDeviceSetting, 
    struct mgos_config_wifi_sta **dirStation
    );

/**
 * @brief Modifica el número de intentos a realizar para conectarse a wifi
 * @param attemps Intentos que deben ser mayores e iguales a 1
 **/
void connect_wifi_modify_attempts(uint8_t attemps);
 
#ifdef __cplusplus
}
#endif

#endif /* ifndef HANDLECOMMUNICATION_H */
