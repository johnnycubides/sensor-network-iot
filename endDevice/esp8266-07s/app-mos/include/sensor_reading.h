/**
 * @file 
 * @brief representación de la lectura de todos los sensores
 * @author Johnny Cubides
 * contact <jgcubidesc@gmail.com>
 * @date Wednesday 25 March 2020
 **/

#ifndef SENSOR_READING_H
#define SENSOR_READING_H

#include "mgos.h"
#include "semva.h"
#include "mgos_si7021.h"
#include "mgos_arduino_adafruit_tsl2591.h"
#include "mgos_arduino_adafruit_stemma_soil.h"

#define ATTEMPS 3 /*!< Número de intentos para acceder a sensores */
#define NOTFOUND -127 /*!< sensor no encontrado */
#define MIN_LUMINOSITY 0 /*!< Mínimo valor esperado para iluminación */
#define MAX_LUMINOSITY 80000 /*!< Máximo valor esperado para iluminación */
#define MIN_SOIL_MOISTURE 0.0 /*!< Minima medida esperada de la capturada por el sensor de humedad el suelo */
#define MAX_SOIL_MOISTURE 110.0 /*!< Máxima medición esperada capaturada por el sensor */
#define MIN_SOIL_TEMPERATURE -10.0 /*!< Míminima temperatura esperada del sensor del suelo */
#define MAX_SOIL_TEMPERATURE 50.0 /*!< Máxmima temperatura esperada del sensor del suelo */
#define MIN_RELATIVE_MOISTURE 0.0 /*!< Mínima humedad relativa esperada */
#define MAX_RELATIVE_MOISTURE 100.0 /*!< Máxima humedad relativa esperada */
#define MIN_RELATIVE_TEMPERATURE -10.0 /*!< Mínima temperatura relativa esperada */
#define MAX_RELATIVE_TEMPERATURE 50.0 /*!< Máxima temperatura relativa esperada */
#define INVALID_VALUE -100 /*!< Si el sensor retorna un valor fuera del rango su valor será -100 */
/* #define VALID_VALUE(MIN, VALUE, MAX) (((MIN<=VALUE)&&(VALUE<=MAX))?VALUE:INVALID_VALUE) /*!< identificar si el valor optenido por el sensor es válido *1/ */ //TODO

/**
 *	Representación de las 6 variables ambientales a medir
 */
/* struct Measurements */
/* { */
/*     float relativeMoisture; /*!< Humedad porcentual dada por si7021 *1/ */
/*     float relativeTemperature; /*!< Temperatura relativa dada por si7021 *1/ */
/*     float soilMoisture; /*!< Humedad porcentual captada por sensor adafruit *1/ */
/*     float soilTemperature; /*!< Temperatura del suelo *1/ */
/*     int32_t luminosity; /*!< Luminosidad captada por sensor tsl *1/ */
/* }; */


/**
 * @brief Configuración requerida para el sensor de iluminación
 */
void cfgSenTsl(void);

/**
 * @brief Dectecta el sensor de hum/temp suelo
 * @return la respuesta será verdadera en el caso de detectarlo
 **/
bool initSeeSaw(void);

/**
 * @brief Detectar sensor y llamar configuración en caso contrario retornar error.
 * @return Retorna verdadero si el sensor es identificado y configurado.
 **/
bool initTsl(void);

/**
 * @brief Dectetar el sensor si7021
 * @return Retorna verdadero si el sensor si7021 es identificado
 **/
bool initSi7021(void);


/**
 * @brief Inicia todos los sensores
 * @param *pMeasurements apuntador a la pila donde se
 * reportará la localización de los sensores
 * @return En *pMeasurements[0] será escrito con un valor NOTFOUND si el
 * el sensor no es localizado
 **/
void initSensors(struct MeasurementStack *pMeasurements);

/**
 * @brief función que lee guarda el valor capturado por los sensores
 * @param *pMeasurements puntero donde se escribirá el resultado de los dato
 * guardados
 * @return Se escribirá en *pMeasurements[0] los valores optenidos por los sensores
 **/
void readSensors(struct MeasurementStack *pMeasurements);

#endif /* ifndef SENSOR_READING_H */
