/**
 * @file 
 * @brief Reponsabilidad de contectar y controlar las
 * comunicaciones del protocolo MQTT implementado
 * @author Johnny Cubides
 * contact jgcubidesc@gmail.com
 * @date Thursday 16 April 2020
 **/

#ifndef HANDLEMQTT_H
#define HANDLEMQTT_H

#include <stdint.h>
#include "semva.h"
#include "mgos.h"
#include "mgos_mqtt.h"
#include "handleCommunication.h"
#include "mgos_nanopb.h"
#include "semva.pb.h"
#include "handleFile.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Configuración y validación del protocolo mqtt
 * @param **dirMeasurementStack apuntador a estructura que contiene
 *  la pila de mediciones
 * @param **dirDeviceSetting Apuntador a las variables de configuración al MQTT
 * @param nameParam Apuntador a la structura que representa la configuración
 * actual de MQTT
 * @return Si valida la configuración
 **/
bool connect_mqtt_config(
    struct MeasurementStack **dirMeasurementStack,
    struct DeviceSettings **dirDeviceSetting,
    struct mgos_config_mqtt **dirMqttClient
    );

/**
 * @brief Función que inicia el proceso de conexión con el broker
 **/
void connect_mqtt(void);

/**
 * @brief Modifica el número de intentos a realizar para conectarse a mqtt
 * @param attemps Intentos que deben ser mayores e iguales a 1
 **/
void connect_mqtt_modify_attempts(uint8_t attemps);


/**
 * @brief Proceso de publicación y actualización de pila
 * en memoria flash
 **/
void mqtt_public_semva_and_update_stack(void);

#ifdef __cplusplus
}
#endif
#endif /* ifndef HANDLEMQTT_H */
