#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	comandos auxiliares
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Tuesday 14 April 2020
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

help(){
 printf "Help for this command mos.sh\n"
  printf "\tmos.sh Command options\n"
  printf "\t[Commands]\n"
  printf "\t\t${GREEN}1${CYAN}\tSerial comunication${NC}\n"
  printf "\t\t${GREEN}2${CYAN}\tGet config global${NC}\n"
  printf "\t\t${GREEN}3${CYAN}\tGet wifi status${NC}\n"
  printf "\t\t${GREEN}4${CYAN}\tmake f s${NC}\n"
  printf "\t\t${GREEN}command3${CYAN}\tbrief3${NC}\n"
  printf "\t\t${GREEN}-h,--help\t${CYAN}Help${NC}\n"
  printf "\nRegards Johnny.\n"
}

help
VAR=""
read -p "Inserte opción: " -r VAR
if [ "$VAR" = "-h" ] || [ "$VAR" = "" ] || [ "$VAR" = "--help" ];
then
  help
elif [ "$VAR" = "1" ];
then
  mos console
elif [ "$VAR" = "2" ];
then
  mos call Sys.GetInfo
elif [ "$VAR" = "3" ];
then
  mos config-get wifi
elif [ "$VAR" = "4" ];
then
  make f && mos console
elif [ "$VAR" = "command3" ];
then
  echo "pass"
fi

