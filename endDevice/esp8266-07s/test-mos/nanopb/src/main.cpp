#include "main.h"

static struct mgos_si7021 *s_si7021;

/* SimpleMessage semva = SimpleMessage_init_zero; */
pb_TempEvent semva = pb_TempEvent_init_zero;

static void timer_cb(void *user_data) {
  (void) user_data;

  float temperature, humidity;
  
  temperature=mgos_si7021_getTemperature(s_si7021);
  humidity=mgos_si7021_getHumidity(s_si7021);

  semva.deviceId = 1;
  semva.eventId = 100; 
  semva.temperature = temperature;
  semva.humidity = humidity;

  /* LOG(LL_INFO, ("si7021 temperature=%.2f humidity=%.2f", temperature, humidity)); */

  uint8_t buffer[128];
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

  bool status = pb_encode(&stream, pb_TempEvent_fields, &semva);
  
  if (!status)
  {
    LOG(LL_INFO, ("Failed to encode"));
  }else
  {
    LOG(LL_INFO, ("Logitud mensaje %d", stream.bytes_written));
    uint8_t i;
    for(i=0; i<stream.bytes_written; i++)
    {
      /* LOG(LL_INFO, ("%02d", buffer[i])); */
      LOG(LL_INFO, ("%x", buffer[i]));
    }
    LOG(LL_INFO, ("si7021 temperature=%.2f humidity=%.2f", temperature, humidity));
  }

  /* if (!pb_encode(&stream, pb_TempEvent_fields, &semva)){ */
    /* LOG(LL_INFO, ("%s",(PB_GET_ERROR(&stream)))); */
  /* } */
  
  /* LOG(LL_INFO, ("%s", buffer)); */

}

enum mgos_app_init_result mgos_app_init(void) {

  LOG(LL_ERROR, ("test"));
  mgos_gpio_setup_output(PIN_DONE, 0);
  struct mgos_i2c *i2c=mgos_i2c_get_global();
  if (!i2c) {
    LOG(LL_ERROR, ("I2C bus missing, set i2c.enable=true in mos.yml"));
  } else {
    s_si7021=mgos_si7021_create(i2c, 0x40); // Default I2C address
    if (s_si7021) {
      mgos_set_timer(5000, true, timer_cb, NULL);
    } else {
      LOG(LL_ERROR, ("Could not initialize sensor"));
    }
  }
  return MGOS_APP_INIT_SUCCESS;
}

