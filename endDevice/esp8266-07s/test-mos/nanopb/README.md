# Ejemplo de protobuffer

## Referencias

[Ejemplo implementado en esp8266](https://medium.com/grpc/efficient-iot-with-the-esp8266-protocol-buffers-grafana-go-and-kubernetes-a2ae214dbd29)

[Implementación en c de proto buffer](https://jpa.kapsi.fi/nanopb/)

[Repositorio de ejemplo de uso de protobuffer](https://github.com/vladimirvivien/iot-dev/tree/master/esp8266/esp8266-dht11-temp)

[nano pb docs](https://jpa.kapsi.fi/nanopb/docs/index.html)

[Ejemplo de dfrobot](https://www.dfrobot.com/blog-1161.html)
