# Prueba de decodificación de nanopb

Herramienta de decodificación online usada:

[https://protogen.marcgravell.com/decode](https://protogen.marcgravell.com/decode)

## Reporte del mongoose os

```bash
Logitud mensaje 14
080110641d9fc6e0412568323b42
si7021 temperature=28.10 humidity=46.80
```

## Reporte del decodificador online

Al ingresal la cadena de datos hexadecimal se obtiene lo siguiente:

```bash
Online Protobuf Decoder.

08 = field 1, type Variant

01 = 1 (raw) or -1 (zigzag)

10 = field 2, type Variant

64 = 100 (raw) or 50 (zigzag)

1D = field 3, type Fixed32

9F-C6-E0-41 = 1105249951 (integer) or 28.0969829559326 (floating point)

25 = field 4, type Fixed32

68-32-3B-42 = 1111175784 (integer) or 46.7992248535156 (floating point)

This tool will pull apart arbitrary protobuf data (without requiring a schema),
displaying the hierarchical content.
```
