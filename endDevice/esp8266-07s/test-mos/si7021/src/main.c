#include "main.h"
#include "mgos.h"

#include "mgos_i2c.h"
#include "mgos_si7021.h"

static struct mgos_si7021 *s_si7021;

static void timer_cb(void *user_data) {
  float temperature, humidity;
  
  temperature=mgos_si7021_getTemperature(s_si7021);
  humidity=mgos_si7021_getHumidity(s_si7021);

  LOG(LL_INFO, ("si7021 temperature=%.2f humidity=%.2f", temperature, humidity));

  (void) user_data;
}

enum mgos_app_init_result mgos_app_init(void) {
  struct mgos_i2c *i2c = mgos_i2c_create(&cfg);
  /* struct mgos_i2c *i2c; // {sda:4, scl:5} */

  mgos_gpio_setup_output(PIN_DONE, 0);
  /* i2c=mgos_i2c_get_global(); */
  if (!i2c) {
    LOG(LL_ERROR, ("I2C bus missing, set i2c.enable=true in mos.yml"));
  } else {
    s_si7021=mgos_si7021_create(i2c, 0x40); // Default I2C address
    if (s_si7021) {
      mgos_set_timer(100, true, timer_cb, NULL);
    } else {
      LOG(LL_ERROR, ("Could not initialize sensor"));
    }
  }
  return MGOS_APP_INIT_SUCCESS;
}

/* static void timer_cb(void *arg) { */
/*   static bool s_tick_tock = false; */
/*   LOG(LL_INFO, */
/*       ("%s uptime: %.2lf, RAM: %lu, %lu free", (s_tick_tock ? "Tick" : "Tock"), */
/*        mgos_uptime(), (unsigned long) mgos_get_heap_size(), */
/*        (unsigned long) mgos_get_free_heap_size())); */
/*   s_tick_tock = !s_tick_tock; */
/* #ifdef USER_LED */
/*   mgos_gpio_toggle(USER_LED); */
/* #endif */
/*   (void) arg; */
/* } */

/* enum mgos_app_init_result mgos_app_init(void) { */
/* #ifdef USER_LED */
/*   mgos_gpio_setup_output(USER_LED, 0); */
/* #endif */
/*   mgos_set_timer(1000 /1* ms *1/, MGOS_TIMER_REPEAT, timer_cb, NULL); */
/*   return MGOS_APP_INIT_SUCCESS; */
/* } */
