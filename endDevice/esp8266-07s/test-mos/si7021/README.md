#

## Update mos

```bash
curl -fsSL https://mongoose-os.com/downloads/mos/install.sh | /bin/bash
```

## Templates

* [template in C](https://github.com/mongoose-os-apps/demo-c)
* [Guia de C](https://mongoose-os.com/docs/mongoose-os/quickstart/develop-in-c.md)

## save files

* [jsonfiles](https://github.com/mongoose-os-libs/jstore/blob/master/include/mgos_jstore.h)
