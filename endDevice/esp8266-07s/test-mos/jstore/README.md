# A blank Mongoose OS app

## Overview

This is an empty app, serves as a skeleton for building Mongoose OS
apps from scratch.

## Revisiones

[Uso de whiles en macros](https://stackoverflow.com/questions/154136/why-use-apparently-meaningless-do-while-and-if-else-statements-in-macros)

[Porqué usar while(0)](https://www.quora.com/What-is-the-use-of-while-1-and-while-0-in-C-programming)

[Uso de __LINE__ en c](https://www.cprogramming.com/reference/preprocessor/__LINE__.html)

[Uso de memset()](https://es.stackoverflow.com/questions/78218/para-que-sirve-el-memset)

[Opaque pointer](https://en.wikipedia.org/wiki/Opaque_pointer)
