#include "main.h"

static int s_num_tests = 0;

// numero de pruebas
#define STRINGIFY_MG_STR(a)                                                    \
  (mg_mk_str_n((STRINGIFY(a)) + 1, strlen(STRINGIFY(a)) - 2))
#define STRINGIFY(a) STRINGIFY2(a)
#define STRINGIFY2(a) #a

#define FAIL(str, line)                                                        \
  do {                                                                         \
    printf("Fail on line %d: [%s]\n", line, str);                              \
    return str;                                                                \
  } while (0)

#define ASSERT(expr)                                                           \
  do {                                                                         \
    s_num_tests++;                                                             \
    if (!(expr))                                                               \
      FAIL(#expr, __LINE__);                                                   \
  } while (0)

#define RUN_TEST(test)                                                         \
  do {                                                                         \
    const char *msg = test();                                                  \
    if (msg)                                                                   \
      return msg;                                                              \
  } while (0)

// Tipo de items esperados del json
struct jstore_expected_item {  
  struct mg_str id; //El identificador puede ser tipo numérico o string
  struct mg_str data; //Valor para cada mensaje
  bool equal; // no sé que és exactamente
};

// Elementos esperados del json almacenado
struct jstore_expected {
  int items_cnt; // es un contador pero aún no es claro si es el total de ellos
  struct jstore_expected_item *items; //apuntador a los elementos dentro del json

  int next_idx; // no hay idea aún de lo que hace
};

static const char *save_items(void){
  printf("here\r\n");
  int i; // iterador auxiliar
  char *err = NULL; //Se trata de in indicador de errores 
  struct mgos_jstore *store = mgos_jstore_create("data.json", &err);
  // Si hay error mostrará el tipo de error de la operación mgos_jstore_create
  if (err != NULL) {
    printf("error: %s\n", err);
  }
  ASSERT(err == NULL);
  // se inicia una estructura para recolectar los datos esperados 
  struct jstore_expected expected;
  memset(&expected, 0, sizeof(expected)); //Según el tamaño antes se pone los valores en 0
  expected.items = calloc(100, sizeof(struct jstore_expected_item));

  /* ASSERT(mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(1), */
  /*                              mg_mk_str("\"foo bar\""), */
  /*                              MGOS_JSTORE_OWN_FOREIGN, &err)); */

  /* ASSERT(mgos_jstore_item_add( */
  /*            store, mg_mk_str("relativeMoisture"), mg_mk_str("\"new data 1\""), */
  /*            MGOS_JSTORE_OWN_FOREIGN, MGOS_JSTORE_OWN_FOREIGN, NULL, NULL, NULL) */
  /*            .p != NULL); */

  struct mg_str myId; 
  struct mg_str myData;
  mgos_jstore_item_hnd_t hnd;
  int index;

  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_ID(mg_mk_str("soilMoisture")),
          NULL, &myData, NULL, NULL, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myId: %s\n", myId);
  printf("myData: %s\n", myData);
  printf("index: %d\n", index);

  mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(0),
                               mg_mk_str("[0.0]"),
                               MGOS_JSTORE_OWN_FOREIGN, &err);
  mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(1),
                               mg_mk_str("[0.0]"),
                               MGOS_JSTORE_OWN_FOREIGN, &err);
  mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(2),
                               mg_mk_str("[0.0]"),
                               MGOS_JSTORE_OWN_FOREIGN, &err);
  mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(3),
                               mg_mk_str("[0.0]"),
                               MGOS_JSTORE_OWN_FOREIGN, &err);
  mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(4),
                               mg_mk_str("[0]"),
                               MGOS_JSTORE_OWN_FOREIGN, &err);
  mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_INDEX(5),
                               mg_mk_str("[0]"),
                               MGOS_JSTORE_OWN_FOREIGN, &err);


  /* mgos_jstore_item_edit(store, MGOS_JSTORE_REF_BY_ID("relativeMoisture"), */
  /*                              mg_mk_str("\"foo bar\""), */
  /*                              MGOS_JSTORE_OWN_FOREIGN, &err); */

  if (err != NULL) {
    printf("error: %s\n", err);
  }


  /* ASSERT(!mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_ID(mg_mk_str("5")), */
  /* ASSERT(!mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_INDEX(5), */
  /*                              NULL, &myData, NULL, NULL, &err)); */
  
  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_INDEX(0),
          &myId, &myData, &hnd, &index, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myId: %s\n", myId);
  printf("myData: %s\n", myData);
  printf("index: %d\n", index);

  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_INDEX(1),
          NULL, &myData, NULL, NULL, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myData: %s\n", myData);

  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_ID(mg_mk_str("soilMoisture")),
          NULL, &myData, NULL, NULL, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myData: %s\n", myData);

  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_INDEX(3),
          NULL, &myData, NULL, NULL, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myData: %s\n", myData);

  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_INDEX(4),
          NULL, &myData, NULL, NULL, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myData: %s\n", myData);

  mgos_jstore_item_get(store, MGOS_JSTORE_REF_BY_INDEX(5),
          NULL, &myData, NULL, NULL, &err);

  if (err != NULL) {
    printf("error: %s\n", err);
  }

  printf("myData: %s\n", myData);

  ASSERT(mgos_jstore_save(store, "data.json", NULL));

  return NULL;
}

static const char *s_run_all_tests(void) {
  printf("start test\r\n");
  RUN_TEST(save_items);
  return NULL;
}

int main(void) {
  const char *fail_msg = s_run_all_tests();
  printf("%s, tests run: %d\n", fail_msg ? "FAIL" : "PASS", s_num_tests);
  return fail_msg == NULL ? EXIT_SUCCESS : EXIT_FAILURE;
}

enum mgos_app_init_result mgos_app_init(void) {
  main();
  return MGOS_APP_INIT_SUCCESS;
}
