/*
 * =====================================================================================
 *
 *       Filename:  main.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/13/2019 06:53:48 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include "mgos.h"
#include <stdio.h>
#include <string.h>
#include "common/cs_file.h"
/* #include "mgos_i2c.h" */
#include "mgos_jstore.h"

#ifndef USER_LED
#define USER_LED 13
#endif /* ifndef USER_LED */
#ifndef PIN_DONE
#define PIN_DONE 14
#endif /* ifndef PIN_DONE */

/* const struct mgos_config_i2c cfg = { */
/*   .enable = true, */
/*   .freq = 400, */
/*   .debug = 0, */
/*   .sda_gpio = 4, */
/*   .scl_gpio = 5 */
/* }; */
