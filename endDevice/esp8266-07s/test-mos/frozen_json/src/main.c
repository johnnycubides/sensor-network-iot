/*
 * Copyright (c) 2014-2018 Cesanta Software Limited
 * All rights reserved
 *
 * Licensed under the Apache License, Version 2.0 (the ""License"");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an ""AS IS"" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "main.h"

static int static_num_tests = 0;

char *data;
const char *filename = "data.json";

float relativeMoisture[LIMIT_DATA];
float relativeTemperature[LIMIT_DATA];
float soilMoisture[LIMIT_DATA];
float soilTemperature[LIMIT_DATA];
uint8_t attemps;

void readData2File(void)
{
    data = json_fread(filename);
    ASSERT(data != NULL);
    printf("data: %s", data);
    /* free(data); // Se requiere liberar memoria manualmente */
}

static void scan_array(const char *str, int len, void *user_data) {
    struct json_token t;
    int i;
    printf("Parsing array: %.*s\n", len, str);
    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) {
        /* char* pEnd; */
        char* pEnd = NULL;
        float temp = strtof(t.ptr, pEnd);
        printf("float %f", temp);
        printf("Index %d, token [%.*s]\n", i, t.len, t.ptr);
    }
}

void getData(void)
{
    uint8_t i;
    void *my_data = NULL;
    /* for(i=0; i<LIMIT_DATA;i++) */
    /* { */
    /*     relativeMoisture[i] = -1; */
    /*     json_scanf(data, strlen(data), ".rm[1]", relativeMoisture[i]); */
    /*     printf("out: %f", relativeMoisture[i]); */
    /* } */
    /* for(i = 0; json_scanf_array_elem(data, strlen(data), ".rm", i, &t) > 0; i++) */
    /* { */
    /*     relativeMoisture[i] = -1; */
    /*     printf("token: %s", t.ptr); */
    /*     json_scanf(t.ptr, t.len, "%f", relativeMoisture[i]); */
    /*     printf("out: %f", relativeMoisture[i]); */
    /* } */
    // si my_data return != NULL -> (const char *str, int len, void *user_data)
    // my_data será los argumentos de scan_array
    json_scanf(data, strlen(data), "{ rm:%M }", scan_array, my_data);
}

// solución adecuada para el contexto
/* double meassuremts[] = {7.1, 7.1, 7.1, 7.1, 7.1, 7.1, 7.0, 7.0, 7.0, 7.0}; */
// problema con el uso de flotantes, se resuelve con double
/* float meassuremts[] = {7.1f, 7.1f, 7.1f, 7.1f, 7.1f, 7.1f, 7.0f, 7.0f, 7.0f, 7.0f}; */
void setData(void){
    /* float meassuremts[] = {7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0}; */
    double meassuremts[] = {7.1, 7.1, 7.1, 7.1, 7.0, 7.222, 7.0, 7.0, 7.0, 7.0};
    json_fprintf(filename, "{ rm:%M }", json_printf_array, meassuremts, sizeof(meassuremts), sizeof(meassuremts[0]), "%f");
}

enum mgos_app_init_result mgos_app_init(void) {
#ifdef LED_PIN
  mgos_gpio_setup_output(LED_PIN, 0);
#endif
  readData2File();
  getData();
  setData();
  return MGOS_APP_INIT_SUCCESS;
}
