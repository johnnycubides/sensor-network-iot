#include "mgos.h"
#include "frozen.h"

#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIMIT_DATA 10

const char *tok_type_names[] = {
    "INVALID", "STRING",       "NUMBER",     "TRUE",        "FALSE",
    "NULL",    "OBJECT_START", "OBJECT_END", "ARRAY_START", "ARRAY_END",
};

#define FAIL(str, line)                                    \
  do {                                                     \
    fprintf(stderr, "Fail on line %d: [%s]\n", line, str); \
    return str;                                            \
  } while (0)

#define ASSERT(expr)                    \
  do {                                  \
    static_num_tests++;                 \
    if (!(expr)) FAIL(#expr, __LINE__); \
  } while (0)

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define RUN_TEST(test)        \
  do {                        \
    const char *msg = test(); \
    if (msg) return msg;      \
  } while (0)

