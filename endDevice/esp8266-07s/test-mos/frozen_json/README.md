# A blank Mongoose OS app

[string to float](http://www.cplusplus.com/reference/cstdlib/strtof/)

[frozen](https://github.com/cesanta/frozen)

[mongoose os frozen](https://mongoose-os.com/docs/mongoose-os/api/core/frozen.h.md)


## Overview

This is an empty app, serves as a skeleton for building Mongoose OS
apps from scratch.

