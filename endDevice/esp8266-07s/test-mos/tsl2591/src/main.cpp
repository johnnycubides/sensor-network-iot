/*
 * Copyright (c) 2014-2018 Cesanta Software Limited
 * All rights reserved
 *
 * Licensed under the Apache License, Version 2.0 (the ""License"");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an ""AS IS"" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mgos.h"
#include "mgos_arduino_adafruit_tsl2591.h"

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);
sensor_t sensor;


void configureSensor(void)
{
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  //tsl.setGain(TSL2591_GAIN_LOW);    // 1x gain (bright light)
  tsl.setGain(TSL2591_GAIN_MED);      // 25x gain
  //tsl.setGain(TSL2591_GAIN_HIGH);   // 428x gain
  
  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  //tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS);  // shortest integration time (bright light)
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS);  // longest integration time (dim light)

  /* Display the gain and integration time for reference sake */  
  /* Serial.println(F("------------------------------------")); */
  /* Serial.print  (F("Gain:         ")); */
  tsl2591Gain_t gain = tsl.getGain();
  switch(gain)
  {
    case TSL2591_GAIN_LOW:
      LOG(LL_INFO, ("1x (Low)"));
      break;
    case TSL2591_GAIN_MED:
      LOG(LL_INFO, ("25x (Medium)"));
      break;
    case TSL2591_GAIN_HIGH:
      LOG(LL_INFO, ("428x (High)"));
      break;
    case TSL2591_GAIN_MAX:
      LOG(LL_INFO, ("9876x (Max)"));
      break;
  }
  LOG(LL_INFO, ("Timing:       "));
  LOG(LL_INFO, ("%d", (tsl.getTiming() + 1) * 100));
  LOG(LL_INFO, (" ms"));
  LOG(LL_INFO, ("------------------------------------"));
}

static void timer_cb(void *arg) {
  static bool s_tick_tock = false;
  LOG(LL_INFO,
      ("%s uptime: %.2lf, RAM: %lu, %lu free", (s_tick_tock ? "Tick" : "Tock"),
       mgos_uptime(), (unsigned long) mgos_get_heap_size(),
       (unsigned long) mgos_get_free_heap_size())); 
  uint16_t light = tsl.getLuminosity(TSL2591_VISIBLE);
  LOG(LL_INFO, ("tsl2591 visible=%d ", light));
  s_tick_tock = !s_tick_tock;
#ifdef LED_PIN
  mgos_gpio_toggle(LED_PIN);
#endif
  (void) arg;
}

enum mgos_app_init_result mgos_app_init(void) {
#ifdef LED_PIN
  mgos_gpio_setup_output(LED_PIN, 0);
#endif
  if (tsl.begin()){
    LOG(LL_INFO, ("Found a TSL2591 sensor"));
    tsl.getSensor(&sensor);
    configureSensor();
    mgos_set_timer(1000 /* ms */, MGOS_TIMER_REPEAT, timer_cb, NULL);
  }else{
    LOG(LL_INFO, ("No sensor found"));
  }
  return MGOS_APP_INIT_SUCCESS;
}
