# Protocol Buffer

## Referencias

[Ejemplo implementado en esp8266](https://medium.com/grpc/efficient-iot-with-the-esp8266-protocol-buffers-grafana-go-and-kubernetes-a2ae214dbd29)

[Nanopb implementación en c de proto buffer](https://jpa.kapsi.fi/nanopb/)

[Repositorio de ejemplo de uso de protobuffer](https://github.com/vladimirvivien/iot-dev/tree/master/esp8266/esp8266-dht11-temp)

## Indicaciones

Usar los siguientes archivos como sigue:

1. `pip install protobuf`
2. `./install-nanoPB.sh` instala en su equipo una versión local del nanopb
3. `. exportPathnanoPB.sh` exporta el PATH del nanopb para usarlo en la consola que lo ha lanzado
