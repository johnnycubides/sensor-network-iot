#!/bin/bash

VERSION=0.3.9.1
TAG=nanopb-$VERSION-linux-x86
TAR=$TAG.tar.gz
DOWNLOAD=https://jpa.kapsi.fi/nanopb/download/$TAR
DESTINATION=$HOME/gitPackages

cd ~/Downloads/
if [[ ! -e $TAR ]]; then
  echo "descargar copia"
  wget -O $TAR "$DOWNLOAD"
else
  echo "existe una copia de protoc en el directorio de descarga"
fi

if [[ ! -d $DESTINATION/$TAR ]]; then
  echo "Descomprimir en opt"
  tar xvf $TAR -C $DESTINATION/
else
  echo "borrar copia anterior y descomprimir en opt"
  rm -rf $DESTINATION/$TAG
  tar xvf $TAR -C $DESTINATION/
fi

if [[ ! -L /usr/local/bin/protoc ]]; then
  echo "Crear enlace simbólico de protoc"
  sudo ln -s $DESTINATION/$TAG/generator-bin/protoc /usr/local/bin/protoc
else
  echo "Remover anterior enlace de protoc y crear nuevo enlace simbólico"
  sudo rm /usr/local/bin/protoc
  sudo ln -s $DESTINATION/$TAG/generator-bin/protoc /usr/local/bin/protoc
fi

if [[ ! -L /usr/local/bin/protoc.bin ]]; then
  echo "Crear enlace simbólico de protoc.bin"
  sudo ln -s $DESTINATION/$TAG/generator-bin/protoc.bin /usr/local/bin/protoc.bin
else
  echo "Remover anterior enlace de protoc.bin y crear nuevo enlace simbólico"
  sudo rm /usr/local/bin/protoc.bin
  sudo ln -s $DESTINATION/$TAG/generator-bin/protoc.bin /usr/local/bin/protoc.bin
fi

read -p "Desea borrar la copia descargada de $TAR: y/n " -r key

if [ "$key" = "y" ];
then
  echo "Se removerá la copia"
  rm $TAR 
else
  echo "Se mantendrá la copia"
fi

