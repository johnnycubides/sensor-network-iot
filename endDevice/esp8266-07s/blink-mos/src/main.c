#include "main.h"
#include "mgos.h"

static void timer_cb(void *arg) {
  static bool s_tick_tock = false;
  LOG(LL_INFO,
      ("%s uptime: %.2lf, RAM: %lu, %lu free", (s_tick_tock ? "Tick" : "Tock"),
       mgos_uptime(), (unsigned long) mgos_get_heap_size(),
       (unsigned long) mgos_get_free_heap_size()));
  s_tick_tock = !s_tick_tock;
#ifdef USER_LED
  mgos_gpio_toggle(USER_LED);
#endif
  (void) arg;
}

enum mgos_app_init_result mgos_app_init(void) {
#ifdef USER_LED
  mgos_gpio_setup_output(USER_LED, 0);
#endif
  mgos_set_timer(1000 /* ms */, MGOS_TIMER_REPEAT, timer_cb, NULL);
  return MGOS_APP_INIT_SUCCESS;
}
