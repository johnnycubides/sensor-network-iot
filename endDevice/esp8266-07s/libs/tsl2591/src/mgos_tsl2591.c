#include "mgos_tsl2591.h"

bool mgos_tsl2591_init(void) {
  return true;
}

/**************************************************************************/
/*!
    @brief  Instantiates a new Adafruit TSL2591 class
    @param  sensorID An optional ID # so you can track this sensor, it will tag
   sensorEvents you create.
*/
/**************************************************************************/

struct MOS_Adafruit_TSL2591 mos_ada_tsl2591;

void Adafruit_TSL2591(int32_t sensorID) {
  mos_ada_tsl2591._initialized = false;
  mos_ada_tsl2591._integration = TSL2591_INTEGRATIONTIME_100MS;
  mos_ada_tsl2591._gain = TSL2591_GAIN_MED;
  mos_ada_tsl2591._sensorID = sensorID;

  // we cant do wire initialization till later, because we havent loaded Wire
  // yet
}

/**************************************************************************/
/*!
    @brief  Setups the I2C interface and hardware, identifies if chip is found
    @param theWire a reference to TwoWire instance
    @returns True if a TSL2591 is found, false on any failure
*/
/**************************************************************************/
/* bool begin(TwoWire *theWire) { // TODO: change */
bool begin(struct mgos_i2c *conn) { // TODO: change
  /* _i2c = theWire; */
  /* _i2c->begin(); */

  /*
  for (uint8_t i=0; i<0x20; i++)
  {
    uint8_t id = read8(0x12);
    Serial.print("$"); Serial.print(i, HEX);
    Serial.print(" = 0x"); Serial.println(read8(i), HEX);
  }
  */

  uint8_t id = read8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_DEVICE_ID);
  if (id != 0x50) {
    return false;
  }
  // Serial.println("Found Adafruit_TSL2591");

  mos_ada_tsl2591._initialized = true;

  // Set default integration time and gain
  setTiming(conn, mos_ada_tsl2591._integration);
  setGain(conn, mos_ada_tsl2591._gain);

  // Note: by default, the device is in power down mode on bootup
  disable(conn);

  return true;
}
/**************************************************************************/
/*!
    @brief  Setups the I2C interface and hardware, identifies if chip is found
    @returns True if a TSL2591 is found, false on any failure
*/
/**************************************************************************/
/* bool begin() { // TODO */

/*   begin(&Wire); */

/*   return true; */
/* } */

/**************************************************************************/
/*!
    @brief  Enables the chip, so it's ready to take readings
*/
/**************************************************************************/
void enable(struct mgos_i2c *conn) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return;
    }
  }

  // Enable the device by setting the control bit to 0x01
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_ENABLE,
         TSL2591_ENABLE_POWERON | TSL2591_ENABLE_AEN | TSL2591_ENABLE_AIEN |
             TSL2591_ENABLE_NPIEN);
}

/**************************************************************************/
/*!
    @brief Disables the chip, so it's in power down mode
*/
/**************************************************************************/
void disable(struct mgos_i2c *conn) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return;
    }
  }

  // Disable the device by setting the control bit to 0x00
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_ENABLE,
         TSL2591_ENABLE_POWEROFF);
}

/************************************************************************/
/*!
    @brief  Setter for sensor light gain
    @param  gain {@link tsl2591Gain_t} gain value
*/
/**************************************************************************/
void setGain(struct mgos_i2c *conn, tsl2591Gain_t gain) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return;
    }
  }

  enable(conn);
  mos_ada_tsl2591._gain = gain;
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_CONTROL, mos_ada_tsl2591._integration | mos_ada_tsl2591._gain);
  disable(conn);
}

/************************************************************************/
/*!
    @brief  Getter for sensor light gain
    @returns {@link tsl2591Gain_t} gain value
*/
/**************************************************************************/
tsl2591Gain_t getGain() { return mos_ada_tsl2591._gain; }

/************************************************************************/
/*!
    @brief  Setter for sensor integration time setting
    @param integration {@link tsl2591IntegrationTime_t} integration time setting
*/
/**************************************************************************/
void setTiming(struct mgos_i2c *conn, tsl2591IntegrationTime_t integration) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return;
    }
  }

  enable(conn);
  mos_ada_tsl2591._integration = integration;
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_CONTROL, mos_ada_tsl2591._integration | mos_ada_tsl2591._gain);
  disable(conn);
}

/************************************************************************/
/*!
    @brief  Getter for sensor integration time setting
    @returns {@link tsl2591IntegrationTime_t} integration time
*/
/**************************************************************************/
tsl2591IntegrationTime_t getTiming() { return mos_ada_tsl2591._integration; }

/************************************************************************/
/*!
    @brief  Calculates the visible Lux based on the two light sensors
    @param  ch0 Data from channel 0 (IR+Visible)
    @param  ch1 Data from channel 1 (IR)
    @returns Lux, based on AMS coefficients (or < 0 if overflow)
*/
/**************************************************************************/
float calculateLux(uint16_t ch0, uint16_t ch1) {
  float atime, again;
  /* float cpl, lux1, lux2, lux; */
  float cpl, lux;
  /* uint32_t chan0, chan1; */

  // Check for overflow conditions first
  if ((ch0 == 0xFFFF) | (ch1 == 0xFFFF)) {
    // Signal an overflow
    return -1;
  }

  // Note: This algorithm is based on preliminary coefficients
  // provided by AMS and may need to be updated in the future

  switch (mos_ada_tsl2591._integration) {
  case TSL2591_INTEGRATIONTIME_100MS:
    atime = 100.0F;
    break;
  case TSL2591_INTEGRATIONTIME_200MS:
    atime = 200.0F;
    break;
  case TSL2591_INTEGRATIONTIME_300MS:
    atime = 300.0F;
    break;
  case TSL2591_INTEGRATIONTIME_400MS:
    atime = 400.0F;
    break;
  case TSL2591_INTEGRATIONTIME_500MS:
    atime = 500.0F;
    break;
  case TSL2591_INTEGRATIONTIME_600MS:
    atime = 600.0F;
    break;
  default: // 100ms
    atime = 100.0F;
    break;
  }

  switch (mos_ada_tsl2591._gain) {
  case TSL2591_GAIN_LOW:
    again = 1.0F;
    break;
  case TSL2591_GAIN_MED:
    again = 25.0F;
    break;
  case TSL2591_GAIN_HIGH:
    again = 428.0F;
    break;
  case TSL2591_GAIN_MAX:
    again = 9876.0F;
    break;
  default:
    again = 1.0F;
    break;
  }

  // cpl = (ATIME * AGAIN) / DF
  cpl = (atime * again) / TSL2591_LUX_DF;

  // Original lux calculation (for reference sake)
  // lux1 = ( (float)ch0 - (TSL2591_LUX_COEFB * (float)ch1) ) / cpl;
  // lux2 = ( ( TSL2591_LUX_COEFC * (float)ch0 ) - ( TSL2591_LUX_COEFD *
  // (float)ch1 ) ) / cpl; lux = lux1 > lux2 ? lux1 : lux2;

  // Alternate lux calculation 1
  // See: https://github.com/adafruit/Adafruit_TSL2591_Library/issues/14
  lux = (((float)ch0 - (float)ch1)) * (1.0F - ((float)ch1 / (float)ch0)) / cpl;

  // Alternate lux calculation 2
  // lux = ( (float)ch0 - ( 1.7F * (float)ch1 ) ) / cpl;

  // Signal I2C had no errors
  return lux;
}

/************************************************************************/
/*!
    @brief  Reads the raw data from both light channels
    @returns 32-bit raw count where high word is IR, low word is IR+Visible
*/
/**************************************************************************/
uint32_t getFullLuminosity(struct mgos_i2c *conn) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return 0;
    }
  }

  // Enable the device
  enable(conn);

  // Wait x ms for ADC to complete
  for (uint8_t d = 0; d <= mos_ada_tsl2591._integration; d++) {
    /* delay(120); */ // TODO

  }

  // CHAN0 must be read before CHAN1
  // See: https://forums.adafruit.com/viewtopic.php?f=19&t=124176
  uint32_t x;
  uint16_t y;
  y = read16(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_CHAN0_LOW);
  x = read16(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_CHAN1_LOW);
  x <<= 16;
  x |= y;

  disable(conn);

  return x;
}

/************************************************************************/
/*!
    @brief  Reads the raw data from the channel
    @param  channel Can be 0 (IR+Visible, 1 (IR) or 2 (Visible only)
    @returns 16-bit raw count, or 0 if channel is invalid
*/
/**************************************************************************/
uint16_t getLuminosity(struct mgos_i2c *conn, uint8_t channel) {
  uint32_t x = getFullLuminosity(conn);

  if (channel == TSL2591_FULLSPECTRUM) {
    // Reads two byte value from channel 0 (visible + infrared)
    return (x & 0xFFFF);
  } else if (channel == TSL2591_INFRARED) {
    // Reads two byte value from channel 1 (infrared)
    return (x >> 16);
  } else if (channel == TSL2591_VISIBLE) {
    // Reads all and subtracts out just the visible!
    return ((x & 0xFFFF) - (x >> 16));
  }

  // unknown channel!
  return 0;
}

/************************************************************************/
/*!
    @brief  Set up the interrupt to go off when light level is outside the
   lower/upper range.
    @param  lowerThreshold Raw light data reading level that is the lower value
   threshold for interrupt
    @param  upperThreshold Raw light data reading level that is the higher value
   threshold for interrupt
    @param  persist How many counts we must be outside range for interrupt to
   fire, default is any single value
*/
/**************************************************************************/
void registerInterrupt(
    struct mgos_i2c *conn,
    uint16_t lowerThreshold, uint16_t upperThreshold,
    tsl2591Persist_t persist) {
  persist = TSL2591_PERSIST_ANY;
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return;
    }
  }

  enable(conn);
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_PERSIST_FILTER, persist);
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_THRESHOLD_AILTL,
         lowerThreshold);
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_THRESHOLD_AILTH,
         lowerThreshold >> 8);
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_THRESHOLD_AIHTL,
         upperThreshold);
  write8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_THRESHOLD_AIHTH,
         upperThreshold >> 8);
  disable(conn);
}

/************************************************************************/
/*!
    @brief  Clear interrupt status
*/
/**************************************************************************/
void clearInterrupt(struct mgos_i2c *conn) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return;
    }
  }

  enable(conn);
  write8(conn, TSL2591_CLEAR_INT, 0x00);
  disable(conn);
}

/************************************************************************/
/*!
    @brief  Gets the most recent sensor event from the hardware status register.
    @return Sensor status as a byte. Bit 0 is ALS Valid. Bit 4 is ALS Interrupt.
   Bit 5 is No-persist Interrupt.
*/
/**************************************************************************/
uint8_t getStatus(struct mgos_i2c *conn) {
  if (!mos_ada_tsl2591._initialized) {
    if (!begin(conn)) {
      return 0;
    }
  }

  // Enable the device
  enable(conn);
  uint8_t x;
  x = read8(conn, TSL2591_COMMAND_BIT | TSL2591_REGISTER_DEVICE_STATUS);
  disable(conn);
  return x;
}

/************************************************************************/
/*!
    @brief  Gets the most recent sensor event
    @param  event Pointer to Adafruit_Sensor sensors_event_t object that will be
   filled with sensor data
    @return True on success, False on failure
*/
/**************************************************************************/
bool getEvent(struct mgos_i2c *conn, sensors_event_t *event) {
  uint16_t ir, full;
  uint32_t lum = getFullLuminosity(conn);
  /* Early silicon seems to have issues when there is a sudden jump in */
  /* light levels. :( To work around this for now sample the sensor 2x */
  lum = getFullLuminosity(conn);
  ir = lum >> 16;
  full = lum & 0xFFFF;

  /* Clear the event */
  memset(event, 0, sizeof(sensors_event_t));

  event->version = sizeof(sensors_event_t);
  event->sensor_id = mos_ada_tsl2591._sensorID;
  event->type = SENSOR_TYPE_LIGHT;
  /* event->timestamp = millis(); */
  event->timestamp = mgos_uptime_micros()/1000;

  /* Calculate the actual lux value */
  /* 0 = sensor overflow (too much light) */
  event->light = calculateLux(full, ir);

  return true;
}

/**************************************************************************/
/*!
    @brief  Gets the overall sensor_t data including the type, range and
   resulution
    @param  sensor Pointer to Adafruit_Sensor sensor_t object that will be
   filled with sensor type data
*/
/**************************************************************************/
void getSensor(sensor_t *sensor) {
  /* Clear the sensor_t object */
  memset(sensor, 0, sizeof(sensor_t));

  /* Insert the sensor name in the fixed length char array */
  strncpy(sensor->name, "TSL2591", sizeof(sensor->name) - 1);
  sensor->name[sizeof(sensor->name) - 1] = 0;
  sensor->version = 1;
  sensor->sensor_id = mos_ada_tsl2591._sensorID;
  sensor->type = SENSOR_TYPE_LIGHT;
  sensor->min_delay = 0;
  sensor->max_value = 88000.0;
  sensor->min_value = 0.0;
  sensor->resolution = 0.001;
}
/*******************************************************/

uint8_t read8(struct mgos_i2c *conn, uint8_t reg) {
  /* uint8_t x; */

  /* _i2c->beginTransmission(TSL2591_ADDR); */
  /* _i2c->write(reg); */
  /* _i2c->endTransmission(); */

  /* _i2c->requestFrom(TSL2591_ADDR, 1); */
  /* x = _i2c->read(); */

  /* return x; */
  return mgos_i2c_read_reg_b(conn, TSL2591_ADDR, reg);
}

uint16_t read16(struct mgos_i2c *conn, uint8_t reg) {
  /* uint16_t x; */
  /* uint16_t t; */

  /* _i2c->beginTransmission(TSL2591_ADDR); */
  /* _i2c->write(reg); */
  /* _i2c->endTransmission(); */

  /* _i2c->requestFrom(TSL2591_ADDR, 2); */
  /* t = _i2c->read(); */
  /* x = _i2c->read(); */

  /* x <<= 8; */
  /* x |= t; */
  /* return x; */
  return mgos_i2c_read_reg_w(conn, TSL2591_ADDR, reg);
}

void write8(struct mgos_i2c *conn, uint8_t reg, uint8_t value) {
  /* _i2c->beginTransmission(TSL2591_ADDR); */
  /* _i2c->write(reg); */
  /* _i2c->write(value); */
  /* _i2c->endTransmission(); */
  mgos_i2c_write_reg_b(conn, TSL2591_ADDR, reg, value);
}

/* void write8(struct mgos_i2c *conn, uint8_t reg) { */
  /* _i2c->beginTransmission(TSL2591_ADDR); */
  /* _i2c->write(reg); */
  /* _i2c->endTransmission(); */
/* } */
