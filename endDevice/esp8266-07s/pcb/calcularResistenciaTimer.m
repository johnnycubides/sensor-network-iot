# load??? yes
# source filename
1;
# Calcular el paralelo
function paralelo = calPar2(a, b)
  paralelo = a*b/(a+b);
endfunction

function Rext = calRExt(M, S)
  T = M*60 + S;
  a = 0.3177;
  b = -136.2571;
  c = 34522.4680;
  Rext  =  100*((-b+sqrt(b^2 - 4*a*(c - 100*T)))/(2*a));
endfunction

