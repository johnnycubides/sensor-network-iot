-- fuentes: http://lua-users.org/wiki/BitwiseOperators
-- fuente http://lua-users.org/wiki/SwitchStatement
-- i2c.setup(id, sda, scl, i2c.SLOW)

-- function read_reg(dev_addr, reg_addr)
--     i2c.start(id)
--     i2c.address(id, dev_addr, i2c.TRANSMITTER)
--     i2c.write(id, reg_addr)
--     i2c.stop(id)
--     i2c.start(id)
--     i2c.address(id, dev_addr, i2c.RECEIVER)
--     c = i2c.read(id, 1)
--     i2c.stop(id)
--     return c
-- end

function lshift(x, by)
  return x * 2 ^ by
end

function rshift(x, by)
  return math.round(x / 2 ^ by)
end


function read_reg(a, b)
  -- return a+b -- suma
  -- return a..b -- concatenar
  return lshift(a,8)+b -- corrimiento
end

read_sensor = {
  ["U5"] = function() return read_reg(0x0A, 0xA0) end,
  ["U6"] = function() return read_reg(0x0B, 0xB0) end
  -- ["ADC"] = function(pin) return adc.read(pin) end
}

print(read_sensor["U5"]())
