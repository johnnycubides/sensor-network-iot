sec, us = 0, 0
wait = tmr.create()
wait:register(10*1000, tmr.ALARM_SINGLE, function()
  sec, us = rtctime.get()        -- show time again before we go to sleep
  print("sec:"..sec.." usec:" .. us)
  print("\n\n\n");
  print("time-to-sleep")
  -- rtctime.dsleep(5*1000, 4)
  rtctime.dsleep(SLEEP_TIME*60000000, 4)
  -- node.dsleep(5*1000, 4);  -- this vesion is documented that it does NOT keep time
end)

function work()
--work = tmr.create()
--work:register(15000, tmr.ALARM_AUTO, function()
  print("work-start")
  -- leyendo todos los sensores
  for i = 1, table.getn(SENSOR) do
      print(SENSOR[i])
      SENSOR_VALUE[i] = read_sensor(SENSOR[i])
      print(SENSOR_VALUE[i])
  end
  -- publicando mensajes
  for i = 1, table.getn(SENSOR) do
    mosquitto_pub(ENDPOINT_NAME.."/"..PUBLIC_TOPICS[i], SENSOR_VALUE[i])
  end
--end)
  --sleep de wifi module
  -- rtctime.dsleep(SLEEP_TIME*60000000, 4)
  print("continue?")

  sec, us = rtctime.get()
  print("sec:"..sec.." usec:" .. us)
  if (sec == 0) then
   rtctime.set(1);  -- to start running the rtc
  end
  wait:start()
end


--work:start()

-- mosquitto_init(BROKER_IP, BROKER_PORT, MQTT_ID, "", "" )
