--i2c.setup(id, sda, scl, i2c.SLOW)
i2c.setup(I2C_ID, SDA, SCL, i2c.SLOW)

function read_reg(dev_addr, reg_addr, len)
  print("start-read-i2c")
  i2c.start(I2C_ID)
  i2c.address(I2C_ID, dev_addr, i2c.TRANSMITTER)
  i2c.write(I2C_ID, reg_addr)
  i2c.stop(I2C_ID)
  i2c.start(I2C_ID)
  i2c.address(I2C_ID, dev_addr, i2c.RECEIVER)
  c = i2c.read(I2C_ID, len)
  i2c.stop(I2C_ID)
  print("value")
  print(c)
  return tonumber(c)
end

function read_sensor(nameSensor)
  if string.find(nameSensor, "ADC") == nil then --no es ADC
    print("i2c-sensor")
    return get_value_sensor_i2c[nameSensor]()
  else
    return get_value_sensor_adc[string.sub(nameSensor,6)](tonumber(string.sub(nameSensor,4,4)))
  end
end

get_value_sensor_i2c = {
  -- ["SI7021T"] = function() return ((175.72*65536.0*read_reg(0x40, 0xE3, 2)/65536.0)-46.85) end,
  ["SI7021T"] = function() 
    read_reg(0x40,0xE5,2)
    return ((175.72/65536.0)-46.85) end,
  ["SI7021H"] = function() return (125.0*read_reg(0x40, 0xE5, 2)/65536.0)-6.0 end
}

get_value_sensor_adc = {
  ['HM0'] = function(pin) return (adc.read(pin)/1024)*100 end --hum suelo
}
