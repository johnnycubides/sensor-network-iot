--i2c.setup(id, sda, scl, i2c.SLOW)
i2c.setup(I2C_ID, SDA, SCL, i2c.SLOW)

function read_reg(dev_addr, reg_addr)
  i2c.start(id)
  i2c.address(id, dev_addr, i2c.TRANSMITTER)
  i2c.write(id, reg_addr)
  i2c.stop(id)
  i2c.start(id)
  i2c.address(id, dev_addr, i2c.RECEIVER)
  c = i2c.read(id, 1)
  i2c.stop(id)
  return c
end

read_sensor = {
  ["S1"] = function() return read_reg(0x00, 0x00) end,
  ["S2"] = function() return read_reg(0x00, 0x00) end,
  ["ADC"] = function(x) return adc.read(x) end
}
