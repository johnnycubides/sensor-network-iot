work = tmr.create()
work:register(15000, tmr.ALARM_AUTO, function()
  -- print("work-start")
  -- leyendo todos los sensores
  for i = 1, table.getn(SENSOR) do
    if string.find(SENSOR[i],"ADC") == nil then
      SENSOR_VALUE[i] = read_sensor[SENSOR[i]]()
    else --ADC
      SENSOR_VALUE[i] = read_sensor["ADC"](tonumber(string.sub(SENSOR[i],4)))
    end
  end
  -- publicando mensajes
  for i = 1, table.getn(SENSOR) do
    mosquitto_pub(ENDPOINT_NAME.."/"..PUBLIC_TOPICS[i], SENSOR_VALUE[i])
  end
end)
--work:start()

-- mosquitto_init(BROKER_IP, BROKER_PORT, MQTT_ID, "", "" )
