#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv, sys
from subprocess import Popen, PIPE, call

ENDPOINT_NAME = 0
PUBLIC_TOPICS = 1
SENSOR = 2
NETWORK = 3
PASS = 4
BROKER_IP = 5
BROKER_PORT = 6
MQTT_ID = 7
SDA = 8
SCL = 9

CHIP_ID = 1

CONFIG = ""

def main():
    print("Haga un reset manual (con el boton de reset) si no da ningun resultado automatico")
    sub = Popen(["esptool.py", "chip_id"], stdout=PIPE)
    ESPTOOL_STDOUT = sub.communicate()[0]
    ESPTOOL_STDOUT = ESPTOOL_STDOUT.split("\n")
    ESPTOOL_STDOUT = ESPTOOL_STDOUT[11]
    ESPTOOL_STDOUT = ESPTOOL_STDOUT.split(" ")
    if ESPTOOL_STDOUT[0] == "Chip" :
        ESPTOOL_STDOUT = ESPTOOL_STDOUT[2]
        print(ESPTOOL_STDOUT)
        THIS_ENPOINT_NAME = get_name_endpoint(ESPTOOL_STDOUT)
        if THIS_ENPOINT_NAME != None :
            if create_config_file(THIS_ENPOINT_NAME) == True:
                while str(raw_input('¿Desea subir los script .lua? y/n: ')) == 'y':
                    upload_files()
                print("Tarea terminada exitosamente")
            else:
                print("Tarea no terminada correctamente")
        else:
            print("Tarea no terminada correctamente")
    else:
        print("Tarea no terminada correctamente")

def get_name_endpoint(chip_id):
    with open('endpoints.csv', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        for row in spamreader:
            if row[CHIP_ID] == chip_id:
                return row[ENDPOINT_NAME]
        return None

def upload_files():
    call(["nodemcu-uploader", "--baud", "115200", "upload", "iot.lua", "read_sensors.lua", "run.lua", "config.lua", "init.lua"])

def create_config_file(endpoint_name):
    status = False
    with open('config.csv', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        for row in spamreader:
            if row[ENDPOINT_NAME] == endpoint_name:
                CONFIG = 'ENDPOINT_NAME = "'+row[ENDPOINT_NAME]+'"\n'
                CONFIG += 'PUBLIC_TOPICS = '+row[PUBLIC_TOPICS]+'\n'
                CONFIG += 'SENSOR = '+row[SENSOR]+'\n'
                CONFIG += 'SENSOR_VALUE = {}\n'
                CONFIG += 'NETWORK = "'+row[NETWORK]+'"\n'
                CONFIG += 'PASS = "'+row[PASS]+'"\n'
                CONFIG += 'BROKER_IP = "'+row[BROKER_IP]+'"\n'
                CONFIG += 'BROKER_PORT = "'+row[BROKER_PORT]+'"\n'
                CONFIG += 'MQTT_ID = '+row[MQTT_ID]+'\n'
                CONFIG += 'I2C_ID = 0\n'
                CONFIG += 'SDA = '+row[SDA]+'\n'
                CONFIG += 'SCL = '+row[SCL]+'\n'
                # print(row)
                # print('#'.join(row))
                status = True
                break
        print(CONFIG)
        f = open("config.lua", "w")
        f.write(CONFIG)
        f.close()
    return status

main()
