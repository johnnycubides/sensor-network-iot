# Guia para el uso de ésta documentación

La documentación presentada a continuación está escrita en MarkDown y puede
ser visualizada en HTMl.

  * Para editar y/o agregar más documentación puede hacerlo en el directorio **docs/**.

  * El `Makefile` es responsable de las tareas de construcción y visualización de la información.
    Puede ver ayudas oprimiendo `make`.

  * El proceso de visualización de la información es el siguiente:

    1.  Abra una terminal y ejecute `make serve`.

    2.  Abra el navegador y entre a la siguiente dirección web `localhost:8000`.

# Instalación de MkDocs

En linux:
```bash
pip install mkdocs
pip install mkdocs-material
```

Atentamente:

Catalejo+ Team

johnnycubides@catalejoplus.com

carolinapulido@catalejoplus.com
