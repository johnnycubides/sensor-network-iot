# Modelo de comunicación

## Esquema general

![Modelo de comunicación](./img/comunication-model/comunication-model.svg)

## (Sigfox)

[Sigfox API](https://support.sigfox.com/apidocs#)

[backend-callbacs](https://build.sigfox.com/backend-callbacks-and-api)

### Características

* Se basa en UNB
* Bandas de frecuencia usadas: USA-902MHz, EU-868MHz
* Los dispositivos conectados a ésta red cuenta con protocolos de encriptamiento VPN portocolo HTTPS
* Recuperación de datos JSON
* La API solo es accesible por HTTPS y todo API endpoints requiere autenticación
* La REST presentada es el GET, POST PUSH, DELETE
* La versión 1 del API solo funcionará hasta el 2020, la versión v2 es BETA
* Sigfox Backend permite aprovisionar devoluciones de llamada personalizadas y enviar datos a las aplicaciones de su servidor

### Servicios sigfox

* Nube y manejo de datos
* Geo-localización y rastreo
* Mensajería

## HTTPS

## MQTT

## HTTPS VS MQTT

## Endpoints

Cualquier elemento que puede leer variables de interes a partir de sensores y comunicarse inalámbricamente para compartir información

### Smart Endpoint

Se trata de un endpoint capaz de comunicarse con el servicio de SEMVA para compartir datos

### Standar Enpoint

Se trata de un endpoint que comparte datos a un concentrador a partir de una red local

## Concentrador

Es un elemento intermedio que recibe información por parte de los endpoint, almacena localmente la misma,
se puede comunicar con el servicio SEMVA para transmitir los datos almacenados y recibir ordenes de configuración
para los endpoints que puede observar.

## Anexo 

### LPWAN

[Referencia](https://www.sciencedirect.com/science/article/pii/S2405959517302953)

#### Comparativa de LPWAN con otras tecnologías

![LPWAN-comparacion](./img/comunication-model/IOT-networks-spectrum-LPWAN-2G-3G-4G-ZIGBEE.jpg)

#### Comparativa de algunas tecnologías LPWAN

![LPWAN comparativa](./img/comunication-model/sigfox-lora-nbiot.jpg)

#### Tabla comparativa de Lora, Sigfox y NB-IOT

|Feature | Sigfox                      | LoRaWAN                                                                                  | NB-IoT                                                                                                             |
|-----------------------------|------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|----------------------------------------|
| Modulation                  | BPSK                                                                                     | CSS                                                                                     | QPSK                                   |
| Frequency                   | Unlicensed ISM bands (868 MHz in Europe, 915 MHz in North America, and 433 MHz in Asia)  | Unlicensed ISM bands (868 MHz in Europe, 915 MHz in North America, and 433 MHz in Asia) | Licensed LTE frequency bands           |
| Bandwidth                   | 100 Hz                                                                                   | 250 kHz and 125 kHz                                                                     | 200 kHz                                |
| Maximum data rate           | 100 bps                                                                                  | 50 kbps                                                                                 | 200 kbps                               |
| Bidirectional               | Limited / Half-duplex                                                                    | Yes / Half-duplex                                                                       | Yes / Half-duplex                      |
| Maximum messages/day        | 140 (UL), 4 (DL)                                                                         | Unlimited                                                                               | Unlimited                              |
| Maximum payload length      | 12 bytes (UL), 8 bytes (DL)                                                              | 243 bytes                                                                               | 1600 bytes                             |
| Range                       | 10 km (urban), 40 km (rural)                                                             | 5 km (urban), 20 km (rural)                                                             | 1 km (urban), 10 km (rural)            |
| Interference immunity       | Very high                                                                                | Very high                                                                               | Low                                    |
| Authentication & encryption | Not supported                                                                            | Yes (AES 128b)                                                                          | Yes (LTE encryption)                   |
| Adaptive data rate          | No                                                                                       | Yes                                                                                     | No                                     |
| Handover                    | End-devices do not join a single base station                                            | End-devices do not join a single base station                                           | End-devices join a single base station |
| Localization                | Yes (RSSI)                                                                               | Yes (TDOA)                                                                              | No (under specification)               |
| Allow private network       | No                                                                                       | Yes                                                                                     | No                                     |
| Standardization             | Sigfox company is collaborating with ETSI on the standardization of Sigfox-based network |                                                                                         |                                        |

### UNB

[Referencia](https://www.mwrf.com/systems/algorithms-antenna-radar-waveform-analysis-using-ambiguity-function)

Transmisión de datos por banda ultra estrecha

#### Comparación en distancia de cobertura

![alcance](./img/comunication-model/UNB-scope.jpg)

#### Comparación entre Narrow Broad

![Tabla comparativa](./img/comunication-model/UNB-table.gif)

#### Relación de señal ruido para UNB versus BB

![UNB](./img/comunication-model/UNB.gif)

### Integración de sigfox con otras pataformas IOT

#### Thingboard IO

[thingboard io](https://thingsboard.io/docs/iot-gateway/sigfox-iot-data-visualization/)

![thingsboard.io](./img/comunication-model/sigfox-gateway-integration.svg)

#### Tago

[Tago](https://tago.elevio.help/en/articles/33-sigfox)

![tago](./img/comunication-model/sigfox-tago.png)

