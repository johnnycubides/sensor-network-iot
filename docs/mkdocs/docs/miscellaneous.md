# Detección de Hardware

### Detección de Dongle Bluetooth

Para identificar un dispositivo bluetooth basta con usar el siguiente comando:

```bash
hciconfig
```

Para identificar la versión de bluetooth que el dispositivo soporta haga como sigue:

```bash
hciconfig hciX version
```

Donde **X** es el número del dispositivo que quiere observar.

**Ejemplo**:

```bash
hciconfig hci0 version
hci0:	Type: Primary  Bus: USB
	BD Address: 38:59:F9:EC:27:45  ACL MTU: 1021:8  SCO MTU: 64:1
	HCI Version: 4.1 (0x7)  Revision: 0x16e7
	LMP Version: 4.1 (0x7)  Subversion: 0x220e
	Manufacturer: Broadcom Corporation (15)
```

Aquí la versión soportada por el dispositivo bluetooth es la 4.1

