# Creación de base de datos

```bash
CREATE DATABASE semva;
CREATE USER admin_semva IDENTIFIED BY 'semva2019';
GRANT USAGE ON *.* TO admin_semva@localhost IDENTIFIED BY 'semva2019';
GRANT ALL privileges ON semva.* TO admin_semva@localhost;
FLUSH PRIVILEGES;
quit;
```
