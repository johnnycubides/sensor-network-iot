# Creación de base de datos para SEMVA

```
CREATE DATABASE semva;
CREATE USER admin_semva IDENTIFIED BY 'semva_2019';
GRANT USAGE ON *.* TO admin_semva@localhost IDENTIFIED BY 'semva_2019';
GRANT ALL privileges ON admin_semva.* TO admin_semva@localhost;
FLUSH PRIVILEGES;
quit;

```
