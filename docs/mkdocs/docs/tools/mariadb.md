# MariaDB

## Comandos básico

[Comandos básicos](https://mariadb.com/kb/es/basic-sql-statements/)

[listar base de datos mariabd mysql](https://www.ochobitshacenunbyte.com/2018/11/15/listar-bases-de-datos-y-usuarios-en-mysql-y-mariadb/)


### Mostrar bases de datos
```bash
SHOW DATABASES;
```

### Usar una base de datos

```bash
USE <name-database>;
```

### Ver las tablas en la base de datos

```bash
SHOW TABLES;
```

### Ver la forma de la base de datos

```bash
DESCRIBE <name-table>;
```

|COMANDO|DESCRIPCIÓN|REQURIMIENTOS|
|:-------------|:-------------|:-----:|
|`SHOW DATABASES;`|Mostrar base de datos||
|`USE <name-database>`|Usar una base de datos||
|`SHOW TABLES;`|Ver tablas en la base de datos||
|`DESCRBE <name-table>;`|Ver la descripción de la base de datos||
|`DROP TABLE <name-table>;`|Elimina la tabla||
||||
||||
