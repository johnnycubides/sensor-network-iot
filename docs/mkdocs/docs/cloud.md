# Sistema en la nube

Se requiere la instalación de los mismos módulos que se encuentran en el HUB.

Aquí se tendrá en cuenta un ejemplo de implementación en la nube con [cloud.google.com](https://cloud.google.com)

## Implementación con cloud Google

### Configuración de FireWall de cloud.google

[Creación de reglas para firewall español](https://cloud.google.com/vpc/docs/using-firewalls?hl=es-419)

[reglas de firewall](https://console.cloud.google.com/networking/firewalls)

A continuación se presentan las reglas usadas en la implementación de cloud google:

![Creación de reglas de firewalls 1](./img/cloud/reglasFireWalls1.png)

En detalle:

![Creación de reglas de firewalls 2](./img/cloud/reglasFireWalls2.png)


