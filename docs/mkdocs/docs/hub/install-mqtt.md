# MQTT

## Instalación de EMQx Broker

[EMQx en light kubernets](https://medium.com/@emqtt/deploying-emq-x-edge-on-raspberry-pi-using-k3s-87c4244612a0)

## Dependencias

* git
* erlang

### Instalando erlang en raspberry

https://elinux.org/Erlang

```bash
sudo apt install wget libssl-dev ncurses-dev
wget http://www.erlang.org/download/otp_src_22.0.tar.gz
tar xvf otp_src_22.0.tar.gz

```
