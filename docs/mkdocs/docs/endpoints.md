Aquí se describirá diferentes configuraciones para las configuraciones de distintos endpoints

# esp8266-e01

## Fuentes

[Guía para configuración del esp8266-e01 en español](https://programarfacil.com/podcast/como-configurar-esp01-wifi-esp8266/)

## nodeMCU (lua)

### Construcción

Se hace uso del constructor de nodeMCU online el cual solicita un correo electrónico para enviar la compilación de 2 versiones
como sigue:

* Integer
* Float

[Constructor online nodeMCU ](https://nodemcu-build.com/)

#### Librerías a compilar

##### Paquete con RTC incompleto

* **ADC**: Leer conversiones ADC
* **bit** Manipulación de bits
* **DHT** Lectura para sensores de humedad y temperatura relativa
* **file**: Manejo del sistema de ficheros en la memoria flash
* **GPIO**: Manejo de pines para propósito digital
* **HTTP**: cliente do GET/POST/PUT/DELETE
* **I2C**: bus de comunicación síncrona hall duplex
* **mDNS**: Sitema para resolver nombres de host en redes que no incluyen un servidor de nombres local *dhcpserver*
* **MQTT**: Cliente 3.1.1
* **net**: TCP/IP cliente servidor
* **node**: Acceso a caracteristicas del sistema
* **RTC time**: Manejo de timer como sleep
* **timer**: Manejo de temporizadores
* **UART**: Transmisor-receptor asíncrono y full-duplex
* **websocket**: Cliente de sockets que implementa RFC6455
* **WiFi**: Manejo de conexiones inalámbricas en los roles: AP, ST, AP_ST

![Módulos seleccionados](./img/endpoints/esp8266-e01/modulos-compilar-nodemcu-online.png)

##### Paquetes RTC funcional

* **ADC**: Leer conversiones ADC
* **bit** Manipulación de bits
* **DHT** Lectura para sensores de humedad y temperatura relativa
* **file**: Manejo del sistema de ficheros en la memoria flash
* **GPIO**: Manejo de pines para propósito digital
* **I2C**: bus de comunicación síncrona hall duplex
* **mDNS**: Sitema para resolver nombres de host en redes que no incluyen un servidor de nombres local *dhcpserver*
* **MQTT**: Cliente 3.1.1
* **net**: TCP/IP cliente servidor
* **node**: Acceso a caracteristicas del sistema
* **One Wire**: Protocolo para sensores un solo hilo
* **RTCMEM**: Manejo de memoria para acciones RTC
* **RTC time**: Manejo de timer como sleep
* **SNTP**: Simple Network Time Protocol
* **timer**: Manejo de temporizadores
* **UART**: Transmisor-receptor asíncrono y full-duplex
* **WiFi**: Manejo de conexiones inalámbricas en los roles: AP, ST, AP_ST

*Notas*:
* HTTP puede ser sustituido por net que tiene uso de TCP
* Se ha quitado websocket lo que impide manejo directo de éste

![Módulos seleccionados](./img/endpoints/esp8266-e01/compile_RTC.png)

### Instalación

![esp8266-e01](./img/endpoints/esp8266-e01/esp8266_esp01_horizontal-01.png)

Para instalar ejecute el script que se encuentra en la ruta `sensor-network-oit/endpoints/esp8266-e01/Makefile`.

#### Instrucciones

![esp8266-e01 en modo flash](./img/endpoints/esp8266-e01/esp8266-e01-flashing.png)

1. Debe poner el hardware en **flashing mode** (ver imagen arriba ↑↑↑)
    1. Debe alimentar el hardware a 3.3V.
    2. Conectar el pin *CHIP_EN* a un 1 lógico (3.3V)
    3. Conectar el pin *GPIO0* a un 0 lógico (0V)
2. Conecte la tarjeta a un adaptador *UART*.
3. Verifique que el tamaño de la memoria flash del esp8266 corresponda al construido por usted con el siguiente comando:
  `make id-flash`
4. Borre la memoria flash para darle formato cuando inicie el esp8266 `make erase-flash`
5. Instalar el firmware nodemcu en esp8266 `make write-flash`

**Observación**: Siempre que ejecute algún comando del esptool.py (en el Makefile) haga **reset** (0 lógico) en la tarjeta.

