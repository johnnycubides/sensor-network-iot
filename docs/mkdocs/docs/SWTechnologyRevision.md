# Revisión de software

[fuente a revisar](https://www.postscapes.com/internet-of-things-software-guide/)

A continuación se hará una revisión de tecnologías tanto
para sistemas embebidos como para bases de datos

| Plataforma  | Sistema               | Chip                             | Lenguaje      | Ram/Rom       | Protocolos     |
|-------------|-----------------------|----------------------------------|---------------|---------------|----------------|
| Lua_RTOS    | freeRTOS              | esp32, esp8266, pic32mz          | Lua           |               | MQTT           |
| elua        | bare-metal            | avr32, cortex-m3, ARM            | Lua           |               | TCP            |
| NodeMCU     |                       | esp                              | Lua           | /512k, 1M     | MQTT, HTTP     |
| Micropython | bare-metal            | esp, stm32f4xx                   | Python 3      | 16KB/256k     | MQTT           |
| Duktape     | engine                | esp32                            | js E5.1       | 64KB/160KB    |                |
| mjs         | bare-metal            | esp, stm[]                       | js ES6 engine | 1KB/50KB      |                |
| jerryjs     | engine, IOT.js        | stm32                            | js            | 64KB/200KB    | HTTP, TCP      |
| Johnny-Five | firmata               | avr8                             | js            | 2KB/32KB      |                |
| Arduino     |                       | avr8, avr32, stm32,esp           | ino:[C, C++]  | 2KB/32KB      |                |
| Pinguino    |                       | pic18, pic32                     | C, C++        |               |                |
| AmForth     |                       | avr8                             | forth         |               |                |
| Flashforth  |                       | avr8, PIC                        | forth         |               |                |
| xForth      |                       | avr, cortex-m, msp430, PIC, STM8 | forth         |               |                |
| ChibiOS     | RTOS                  | avr8, stm32, sam                 | C, C++        |               |                |
| freeRTOS    | RTOS                  | avr8, stm32, sam                 | C, C++        |               |                |
| miskatino   |                       | avr8, stm32                      | Basic         |               |                |
| TinyBasic   | vm                    | av8                              | Basic         |               |                |
| Zerynth     | vm, FreeRTOS, ChibiOS | esp, stm, sam                    | C/Python      | 3-5kB/60-80KB |                |
| CoCo vm     | vm, engine            | PC                               | Python        |               |                |
| Contiki     | S.O. RTOS             | avr8, TI CC, stm, PIC            | C             | 2KB/40KB      | TCP            |
| Mongose     |                       | stm, esp, TI cc                  | js mJS        |               | OTA            |
| RIOT        |                       | avr, sam, stm, esp, arm          |               |               | COAP, MQTT     |
| TinyOs      | O.S.                  | avr, msp                         | NesC          |               |                |
| eCOS        | O.S.                  | stm32, atmel, intel, multiples   | C             |               |                |
| LiteOS      | RTOS huawei           | arm, msp                         |               |               |                |
| BeRTOS      | RTOS                  | sam, avr, stm32                  | C             |               |                |
| NuttX       | O.S. Old              | ARM,                             | C, C++        |               |                |
| Mynewt      | O.S.                  | multiples                        | C             |               | TCP, BLE, LoRa |
| Mbed        | O.S.                  | ARM 32bits                       | C             |               |                |
| Nano-RK     | O.S. Old              |                                  |               |               |                |
| RTEMS       | RTOS                  |                                  |               |               |                |

