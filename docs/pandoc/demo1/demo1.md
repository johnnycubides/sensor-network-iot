---
title: "Demo 1: Red de sensores WiFi conectada a concentrador"
author: Johnny Cubides
date: Thursday 23 May 2019
geometry: margin=2cm
output: pdf_document
---

# ¿ Qué se va a hacer?

Se desarrollará un sistema de adquisición y almacenamiento de datos
de variables ambientales, donde se contará con:

* 1 concentrador de información con capacidad de almacear los datos adquiridos por los endpoints.
* 6 Enpoints que constan de un sistema de comunicación inalámbrico y sensores con el protocolo i2c.

# ¿Con qué se va a hacer?

![Esquema General del Demo 1](./img/esquemaGeneralDemo1.svg)

A continuación se da detalles sobre los elementos mostrados en la imagen
anterior.

## Concentrador

Se hará uso de un sistema embebido que permita a otros dispositivos
conectarse a él (en rol punto de acceso AP) y compartir información por el medio inalámbrico WiFi.

### Hardware

* Raspberry Pi3 B

### Software

|Software|Responsabilidad|Observación|
|:----------:|:-------------:|:-----:|
|GNU/Linux Raspbian-lite|Gestionar los medios de comunicación del sistema|  |
|Mqtt en nodejs|Protocolo de comunicación con los endpoints||
|database^I^|Registrar la actividad y la información almacenada por los endpoints|Observación|

* I. Se requiere definir la base de datos adecuada, sin embargo, se hará una prueba con influxdb.

## Endpoints

El recurso a usar es un sistema embebido capaz de levantar un interprete (Lua o Python), capturar datos de sensores por medio de el protocolo i2c y
con la capacidad de transmitir datos por un medio WiFi cumpliendo el rol de estación (ST).

### Hardware

|Hardware|Responsabilidad|Observación|
|:----------:|:-------------:|:---------:|
|esp8266-1|Permitir la adquisición de los datos de los sensores conectados y facilitar la conexión con el concentrador||
|Regulador-lineal 3.3V|Mantener la tensión requerida para los endpoints|Se hará uso de reguladores previamente creados|
|Sensor de Humedad|Leer la inforamción de la humedad relativa y compartirla por i2c||

### Software

Se hará uso de nodeMCU o Micropython con soporte TCP, MQTT e i2c.

## Tentativo de elementos requeridos para el concentrador y los 6 endpoints

|Elemento|Característica|requeridos|inventario| faltantes|
|:---:|:---:|:---:|:---:|:---:|
|Raspberry Pi|Pi3 B|1|2|0|
|Cardador Raspberry| 5v@2A|1|1|0|
|microSD|8GB|1|1|0|
|Cable Ethernet||1|0|1|
|Esp8266|versión -01|6|7|0|
|Reguladores de 3.3 V|acondicionados|6|7^I^|0|
|Batería|3.7 V|12|12|0|
|Conectores rápidos|H-Mx4^III^|30|0|30|
|Humedad/temperatura|i2c|2|2^II^|0|
|Sensor de Luz|i2c|3|4^II^|0|
|Cases|para endpoints|6^IV^|2|4|
||||||

Observaciones de la tabla anterior:

* I. Los reguladores deben ser revisados para verificar su correcto funcionamiento.

* II Se requiere los datasheet de éstos sensores para el protocolo i2c.

* III Conectores rápidos hembra macho.

* IV Elementos opcionales.

# ¿Qué se espera?

* Módulos esps capaces de adquirir datos de los registros de los sensores a través de I2C.

* Seis endpoints capaces de conectarse al concentrador a través de MQTT para enviar información.

* Registrar en la base de datos instanciada en el concentrador las publicaciones de los endpoints.

* Ver los datos registrados de los endpoints en el concentrador.

# ¿Qué se entrega?

Un sistema de medición y adquisición de datos de variables ambientales que cuenta con un concentrador y seis elementos finales.

* 6 endpoints como sigue:
  * Capacidad de adquirir datos de sensores por el protocolo i2c por software.

# ¿Cuando se entrega?

Fecha de entrega: Viernes 31 de Mayo de 2019, oficina del profesor Sofrony.

# Lista de actividades

Las actividades quedarán registradas en un tablero de Trello como sigue:

* Las actividades en la lista *TODO* son las que se desarrollarán para la fecha de entrega.
* Las actividades en el *BACKLOG* son aquellas que se esperan realizar en un próximo acuerdo de entrega.

El link para revisar las tareas asignadas y su proceso se puede encontrar en el siguiente link:

[Tablero TRELLO](https://trello.com/b/2zC29HMT)
: https://trello.com/b/2zC29HMT

# Revisión

Se espera revisión del documento enviando observaciones y sugerencias al correo electrónico *jgcubidesc@gmail.com*.
