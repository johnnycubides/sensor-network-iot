#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	Instalador de InfluxDB, Telegraf, Grafana
# Author: Johnny Cubides
# e-mail: <johnnycubides@catalejoplus.com> <jgcubidesc@gmail.com>
# date: Wednesday 29 May 2019
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

install_influxdb(){
  curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
  sudo echo "deb https://repos.influxdata.com/debian stretch stable" > /etc/apt/sources.list.d/influxdata.list
  sudo apt-get update
  sudo apt-get install influxdb
  systemctl start influxdb
}

install_telegraf(){
  #curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
  #echo "deb https://repos.influxdata.com/debian stretch stable" > /etc/apt/sources.list.d/influxdata.list
  #apt-get update
  apt install telegraf
  systemctl start telegraf
}

install_grafana(){
  # sudo echo "deb https://packagecloud.io/grafana/stable/debian/ stretch main" > /etc/apt/sources.list.d/grafana.list
  curl https://packages.grafana.com/gpg.key | sudo apt-key add -
  sudo apt install apt-transport-https
  sudo apt update
  sudo apt install grafana
}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
    printf "Help for this command install-all.sh\n"
    printf "\tinstall-all.sh Command options\n"
    printf "\t[Commands]\n"
    printf "\t\ttelegraf\tbrief1\n"
    printf "\t\tinfluxdb\tbrief2\n"
    printf "\t\tgrafana\tbrief3\n"
    printf "\t\t-h,--help\tHelp\n"
    printf "\nRegards Johnny.\n"
elif [ "$1" = "telegraf" ];
then
  read -p "¿Instalar telegraf? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    install_telegraf
  fi
elif [ "$1" = "influxdb" ];
then
  read -p "¿Instalar influxDB? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    install_influxdb
  fi
elif [ "$1" = "grafana" ];
then
  read -p "¿Instalar grafana? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    install_grafana
  fi
fi

