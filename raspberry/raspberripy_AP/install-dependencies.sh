#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	Configuración AP
# Author: Johnny Cubides
# e-mail: <jgcubidesc@gmail.com> <johnnycubides@catalejoplus.com>
# date: Tuesday 28 May 2019
status=$?

# referencias:
# raspberry hostapd config: https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md
# bloqueo de wifi por soft: https://www.raspberrypi.org/forums/viewtopic.php?t=146198
#   solucion? rfkill list all, sudo rfkill unblock all
#   otra solución planteada: https://www.raspberrypi.org/forums/viewtopic.php?t=206223
#     sudo rfkill unblock 0 

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

CONFIG_OLD=./config_old

install_dep_AP(){
  sudo apt upgrade -y
  sudo apt-get install dnsmasq hostapd bridge-utils -y
  sudo systemctl stop dnsmasq
  sudo systemctl stop hostapd
}

configuring_a_static_IP(){
  if [[ ! -e $CONFIG_OLD/dhcpcd.conf_old ]]; then
    sudo cp /etc/dhcpcd.conf $CONFIG_OLD/dhcpcd.conf_old
  fi
  sudo cp ./dhcpcd.conf /etc/dhcpcd.conf
  sudo systemctl restart dhcpcd
  echo $status
}

configuring_the_DHCP_server(){
  #Configuring the DHCP server (dnsmasq)
  if [[ ! -e $CONFIG_OLD/dnsmasq.conf_old ]]; then
    sudo cp /etc/dnsmasq.conf $CONFIG_OLD/dnsmasq.conf_old
  fi
  sudo cp ./dnsmasq.conf /etc/dnsmasq.conf
  sudo systemctl reload dnsmasq
}

configuring_the_access_point_host_software(){
  #Configuring the access point host software (hostapd)
  if [[ ! -e $CONFIG_OLD/hostapd.conf_old ]]; then
    if [[ -e /etc/hostapd/hostapd.conf ]]; then
      sudo cp /etc/hostapd/hostapd.conf $CONFIG_OLD/hostapd.conf_old
    fi
  fi
  sudo cp ./hostapd.conf /etc/hostapd/hostapd.conf
  if [[ ! -e $CONFIG_OLD/hostapd_old ]]; then
    sudo cp /etc/default/hostapd $CONFIG_OLD/hostapd_old
  fi
  sudo cp ./hostapd /etc/default/hostapd
}

start_it_up(){
  sudo systemctl unmask hostapd
  sudo systemctl enable hostapd
  sudo systemctl start hostapd
  if [[ ! -e $CONFIG_OLD/sysctl.conf ]]; then
    sudo cp /etc/sysctl.conf $CONFIG_OLD/sysctl.conf_old
  fi
  sudo cp ./sysctl.conf /etc/sysctl.conf
  sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
  sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
  if [[ ! -e /etc/rc.local ]]; then
    sudo cp /etc/rc.local $CONFIG_OLD/rc.local_old
  fi
  sudo cp ./rc.local /etc/rc.local
}

bridgeInternet(){
  # TODO debe agregarse la configuración de bridge
  # para que los dispositivos tengan acceso 
  sudo systemctl stop hostapd

}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
    printf "Help for this command FILE_NAME\n"
    printf "\tFILE_NAME Command options\n"
    printf "\t[Commands]\n"
    printf "\t\ti-dep-ap\tInstalación de dependecias para Access Point\n"
    printf "\t\tconfig\tConfigurar raspberry como Access Point\n"
    printf "\t\tcommand3\tbrief3\n"
    printf "\t\t-h,--help\tHelp\n"
    printf "\nRegards Johnny.\n"
elif [ "$1" = "i-dep-ap" ];
then
  install_dep_AP
elif [ "$1" = "config" ];
then
  mkdir -p $CONFIG_OLD/
  read -p "¿Configurar una IP statica? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    configuring_a_static_IP
  fi
  read -p "¿Configurar el DHCP server (dnsmasq)? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    configuring_the_DHCP_server
  fi
  read -p "¿Configurar el acces point (hostapd)? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    configuring_the_access_point_host_software
  fi
  read -p "¿Iniciar esto y configurar las máscaras? ${YELLOW}Y${NC}/${RED}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
  then
    start_it_up
  fi
elif [ "$1" = "command3" ];
then
    echo "pass"
fi

