#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	script de instalación de raspbian en raspberry pi
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Wednesday 15 April 2020
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

RASPBERRY_ZIP=raspbian-buster-lite.zip
LINK_RASPBIAN_DOWNLOAD=https://downloads.raspberrypi.org/raspbian_lite_latest

downloadRaspbianZip(){
  wget -O $RASPBERRY_ZIP "$LINK_RASPBIAN_DOWNLOAD"
}

micro_sd=""
p1=""
p2=""

cpImage2mSD(){
  sudo unzip -p $RASPBERRY_ZIP | sudo dd of=$micro_sd conv=fsync status=progress
}

detecteMicroSD(){
  if [ -b /dev/sdb ]; then
    micro_sd=/dev/sdb
    p1=1
    p2=2
  elif [ -b /dev/mmcblk0 ]; then
    micro_sd=/dev/mmcblk0
    p1=p1
    p2=p2
  else
    print " ${RED}No existe una micro SD con la cual trabajar${NC}\n"
    exit 1
  fi
}

installRaspbian(){
  detecteMicroSD
  read -p " ${YELLOW}Verifique que vaya a usar la tarjeta SD en cuestion $micro_sd
  ya que después de éste proceso el contenido de la micro SD no será recuperable${NC}
  desea continuar? y/n: " -r variable

  if [ "$variable" = "y" ];
  then
    cpImage2mSD
  fi
}

enable_ssh_image_raspbian_in_microsd(){
  detecteMicroSD
  printf "${YELLOW}Compruebe primero que la memoria está desmontada para poder continuar${NC}\n"
  lsblk
  read -p "¿La memoria está desmontada? ${GREEN}Y${NC}/${CYAN}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "y" ] || [ "$KEYBOARD" = "Y" ];
  then
    mkdir -p /tmp/mySD
    printf "${YELLOW}Se requiere permiso de administrador para ésta operación${NC}\n"
    NUMBER=$p1
    printf "${CYAN}Montando partición $NUMBER${NC}\n"
    sudo mount $micro_sd$p1 /tmp/mySD
    # mount $micro_sd$p1 /tmp/mySD
    sleep 3
    printf "${CYAN}${NC}\n"
    sudo touch /tmp/mySD/ssh
    # touch /tmp/mySD/ssh
    sleep 2
    sudo umount /tmp/mySD
    # umount /tmp/mySD
    printf "${CYAN}Tarea terminada${NC}\n"
  fi
}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
  printf "Help for this command installRaspbian.sh\n"
  printf "\tinstallRaspbian.sh Command options\n"
  printf "\t[Commands]\n"
  printf "\t\td\tDescargar la última versión de raspbian\n"
  printf "\t\ti\tIntalar raspbian en raspberry pi\n"
  printf "\t\tssh\thabilitar ssh\n"
  printf "\t\t-h,--help\tHelp\n"
  printf "\nRegards Johnny.\n"
elif [ "$1" = "d" ];
then
  downloadRaspbianZip
elif [ "$1" = "i" ];
then
  installRaspbian
elif [ "$1" = "ssh" ];
then
  enable_ssh_image_raspbian_in_microsd
fi

