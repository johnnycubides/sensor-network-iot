# Instalación y configuración de mosquitto como puente

## Referencias

[Instalación de mosquitto broker en raspberry pi](https://randomnerdtutorials.com/how-to-install-mosquitto-broker-on-raspberry-pi/)

## Instalación de mosquitto broker

```bash
sudo apt update
sudo apt install -y mosquitto
sudo systemctl enable mosquitto.service
```

* `mosquitto -v` -> verifica que esté operando mosquitto
* `hostname -I` -> Identificar las *IPs* por donde escucha el broker

```bash
/etc/mosquitto/conf.d
cat bridge.conf 
connection emqx_semva

address 35.237.235.140:1883
bridge_protocol_version mqttv311
#remote_username user
#remote_password passwd
topic sensor/# out 1
topic control/# in 1
sudo service mosquitto restart
```

**Observación**: si el comando *mosquitto -v* genera el ERROR: *Address already in use* no significa que hay un problema,
lo que indica es que el mosquitto ya está usando el puerto por default


**Observación**: para hacer pruebas con clientes se puede instalar `mosquitto-clients`

## Configuración como BRIGE


## Conexiones SSL

[Tutorioal de configuración moquitto ssl bridge](http://www.steves-internet-guide.com/mosquitto-bridge-encryption/)

[Configuración ssl para cliente comsquito](http://www.steves-internet-guide.com/mosquitto-tls/)

[Bridge between two Mosquitto brokers](https://codebayblog.wordpress.com/2016/07/17/first-blog-post/)

