# sensor-network-OIT

Se trata ed una implementación de un sistema de IOT que contempla
las capas de EDGE, FOG y CLOUD.

![Arquitectura general](./architecture.png)

Para la capa de EDGE se construye una placa que pueda operar a través
de sleep por hardware y con Mongoose OS para el control de las tareas
de adquisición y transporte de datos. Para la capa FOG se considera
el uso de MQTT y finalmente en la nube se hace uso de Grafana

![Dispositivo final](./endDevice.png)

El dispositivo creado es desarrollado con la herramienta KiCad

Johnny Cubides
