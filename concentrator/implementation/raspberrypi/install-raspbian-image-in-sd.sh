#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	Instalador de imagen raspbian en microSD
# Author: Johnny Cubides
# e-mail: <jgcubides@gmail.com> <johnnycubides@catalejoplus.com>
# date: Wednesday 22 May 2019
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

# Constantes
PATH_SD=
DOWNLOAD_RASPBIAN_URL=https://downloads.raspberrypi.org
RASPBIAN_TORRENT=raspbian_lite_latest.torrent
RASPBIAN_DIR=raspbian-dir
RASPBIAN=raspbian_lite_latest

INFORMATION="Para que éste script funcione correctamente se requiere la instalación adicional de los siguientes programas:
  tansmission-cli.
${YELLOW}Atención!!! Desconecte cualquier memoria microSD que no vaya usar, ésto con el fin de no borrar memorias
que no estén dentro del proceso de crear la imagen de Raspbian${NC}"

enable_ssh_image_raspbian_in_microsd(){
  get_path_microSD
  printf "${YELLOW}Compruebe primero que la memoria está desmontada para poder continuar${NC}\n"
  read -p "¿La memoria está desmontada? ${GREEN}Y${NC}/${CYAN}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "y" ] || [ "$KEYBOARD" = "Y" ];
  then
    mkdir -p /tmp/mySD
    printf "${YELLOW}Se requiere permiso de administrador para ésta operación${NC}\n"
    NUMBER=1
    printf "${CYAN}Montando partición $NUMBER${NC}\n"
    sudo mount $PATH_SD$NUMBER /tmp/mySD
    sleep 3
    printf "${CYAN}${NC}\n"
    sudo touch /tmp/mySD/ssh
    sleep 2
    sudo umount /tmp/mySD
    printf "${CYAN}Tarea terminada${NC}\n"
  fi
}

copy_image_in_microsd() {
  get_path_microSD
  printf "${YELLOW}Compruebe primero que la memoria está desmontada para poder continuar${NC}\n"
  read -p "¿La memoria está desmontada? ${GREEN}Y${NC}/${CYAN}N${NC}: " -r KEYBOARD
  if [ "$KEYBOARD" = "y" ] || [ "$KEYBOARD" = "Y" ];
  then
    printf "${YELLOW}Está apunto de copiar la imagen en el memoria microSD que se encuentra en el path $PATH_SD
después de éste punto los cambios realizados en la memoria microSD serán irreversibles.${NC}\n"
    read -p "¿Desea continuar con la copia de la imagen de Raspbian en la microSD? ${YELLOW}Y${NC}/${CYAN}N${NC}: " -r KEYBOARD
    if [ "$KEYBOARD" = "Y" ] || [ "$KEYBOARD" = "y" ];
    then
      # umount $PATH_SD
      sudo unzip -p $RASPBIAN_DIR/$RASPBIAN.zip | sudo dd of=$PATH_SD bs=4M conv=fsync status=progress
    else
      printf "${CYAN}Operación cancelada${NC}\n"
    fi
  else
    printf "${CYAN}Operación cancelada${NC}\n"
  fi
 }

get_path_microSD() {
  TEMP=( "/dev/mmcblk0" "/dev/sdb" )
  for (( i = 0; i < ${#TEMP[@]} ; i++ )); do
    if [[ -e ${TEMP[i]} ]]; then
      PATH_SD=${TEMP[i]}
      echo $PATH_SD
      break
    fi
  done
}

aux_download_raspbian_from_torrent() {
  printf "${CYAN}Inicia la descarga del torrent${NC}\n"
  wget -O $RASPBIAN_DIR/$RASPBIAN_TORRENT "$DOWNLOAD_RASPBIAN_URL/$RASPBIAN_TORRENT"
  printf "${CYAN}Inicia el proceso de descarga de la imagen de Raspbian${NC}\n"
  # transmission-cli $RASPBIAN_DIR/$RASPBIAN_TORRENT -w $RASPBIAN_DIR/
  transmission-cli $RASPBIAN_DIR/$RASPBIAN_TORRENT -w ./$RASPBIAN_DIR/
  echo "$status"
  echo `pwd`
}

aux_download_raspbian(){
  printf "${CYAN}Inicia la descarga de Raspbian${NC}\n"
  wget -O $RASPBIAN_DIR/$RASPBIAN.zip "$DOWNLOAD_RASPBIAN_URL/$RASPBIAN"
  printf "${CYAN}Rapbian descargado${NC}\n"
}

download_raspbian() {
  if [[ -e $RASPBIAN_DIR/$RASPBIAN.zip ]]; then
    read -p "¿Existe una copia anterior de una imagen raspbian, desea descargar una nueva?: ${RED}Y${NC}/${GREEN}N${NC}: " -r KEYBOARD
    if [ "$KEYBOARD" = "y" ] || [ "$KEYBOARD" = "Y" ];
    then
      echo "remove"
      aux_download_raspbian
    else
      printf "${CYAN}Se mantiene la copia anterior de la imagen Raspbian${NC}\n"
    fi
  else
    aux_download_raspbian
  fi
}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
  printf "Help for this command install-raspbian-image-in-sd.sh\n"
  printf "\tinstall-raspbian-image-in-sd.sh Command options\n"
  printf "\t[Commands]\n"
  printf "\t\tdownload\tDescarga una versión de raspbian\n"
  printf "\t\tcp-img\tcopia la imagen de raspbian en la microSD\n"
  printf "\t\tsh\tHabilita el servidor ssh en Raspbian\n"
  printf "\t\t-h,--help\tHelp\n"
  printf "\nRegards Johnny.\n"
elif [ "$1" = "download" ];
then
  download_raspbian
elif [ "$1" = "cp-img" ];
then
  copy_image_in_microsd
elif [ "$1" = "sh" ];
then
  enable_ssh_image_raspbian_in_microsd
fi

